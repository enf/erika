## Erika Weekly StandUp 

**Links to Key Resources**__
 - Duo Workflow Guardrails - https://gitlab.com/gitlab-org/ux-research/-/issues/3281
 - [CI/CD Research Prioritisation - FY26 ](https://gitlab.com/gitlab-org/ux-research/-/issues/3314)
 - [Global UX Research Requests Intake](https://gitlab.com/gitlab-org/ux-research/-/issues/3336) 

 
### V&P Deep Research Goals - Weekly Changes in Focus

|  Goal  | Prioritization | Supporting Issues / Projects |
| ------ | ------ | ------ |
| Strategic Research on AI | HEAVY Support  | https://gitlab.com/groups/gitlab-org/-/epics/13986 Duo Workflow Early Concept Interviews + Workflow Deep Dive Interviews  + AI and Compliance https://gitlab.com/gitlab-org/ux-research/-/issues/3162 |
| User Product Priorities | Light Support  | [DevOps Portal / IDP Research](https://gitlab.com/gitlab-org/ux-research/-/issues/2994) -analysis complete working on a slide deck / report  [GitLab Adoption Barriers Research](https://gitlab.com/gitlab-org/ux-research/-/issues/2846)  [Kano Survey for Runner Features FY 25 RoadMap Planning](https://gitlab.com/gitlab-org/ux-research/-/issues/2641) |
| User Security Needs | Light Support |  [Native Secrets Solution Early Adopters Program](https://gitlab.com/gitlab-org/ux-research/-/issues/2470) + [Native Secrets Solution Design Sprints at KubecCon NA](https://gitlab.com/gitlab-org/ux-research/-/issues/2471) |
| Enterprise Users | Medium Support | AI and Compliance https://gitlab.com/gitlab-org/ux-research/-/issues/3162  [Native Secrets Solution Early Adopters Program](https://gitlab.com/gitlab-org/ux-research/-/issues/2470)  Enterprise cohort included in  secrets + infrastructure lifecycles [1873](https://gitlab.com/gitlab-org/ux-research/-/issues/1873) + CI Template + [Secrets Enterprise Case Studies](https://gitlab.com/gitlab-org/ux-research/-/issues/1953) + [Enterprise Company Profiles + Personas](https://gitlab.com/gitlab-org/ux-research/-/issues/2017) + [Enterprise Defining Success Study](https://docs.google.com/presentation/d/1k9fJoqgkPX2zaK2-yQKydc_Xl3W44u1GIZNLUjQqZ5k/edit?usp=sharing) |
| User Goals & Workflows | On Hold | [Exploring the DevOps Loop](https://gitlab.com/gitlab-org/ux-research/-/issues/2775) +  [GitLab and "the Cloud"](https://gitlab.com/gitlab-org/ux-research/-/issues/2630) + [AI Runner Optimization[Verify AI Design Pattern Workshops](https://gitlab.com/gitlab-org/ux-research/-/issues/2461) |
 
 ### Other Projects / Issues
  - SecOps Notes / Learnings from Remote SecOps Conference Presentations [#1667] (https://gitlab.com/gitlab-org/ux-research/-/issues/1667)
  - AI "Lit Review" notes on and links to articles on AI and user needs -> https://gitlab.com/gitlab-org/ux-research/-/issues/2549 
  - [UXR Team Article] What we learned from a drop in NPS? -> https://gitlab.com/gitlab-org/ux-research/-/issues/2635 

### Task Backlog
1. CAB Research Issue Template
1. Conference Data Collection Tips 
1. Report -> Verify and Package Research Registry & Synthesis: Research in Review (include benchmark findings)
1.Multi AI Agent Systems with crewAI https://learn.deeplearning.ai/courses/multi-ai-agent-systems-with-crewai/lesson/1/introduction 

## Week of 2025-03-03
1. Q1 Research prioritization for Duo Workflow 
2. Respond to Sarah's questions about her monetization survey
3. Resond to Sam's questions about CI Steps research -> https://gitlab.com/gitlab-org/ux-research/-/issues/3007#note_2376333395 
4. Meet with Nick about formative UX study for Duo Workflow
5. Project Pelican discussion guide review

## Week of 2025-02-24
✅. Meet with Sunjung and Viktor about CI Steps research 

✅. meet with Sarah D about pricing research around Duo Workflow 

✅. Read over CI/CD Plan  - [Accelerating customer value and innovation with GitLab CI/CD - FY26](https://docs.google.com/presentation/d/1AqRFxzPoUqU-TLOUfJMZZEZFXUlwi9kTjDaAjcJdPm0/edit?usp=sharing)

✅. provide a summary of what we know so far for https://gitlab.com/gitlab-org/ux-research/-/issues/3371 

✅. Created a summary of research for Package to support Annabell's work / transition -> https://gitlab.com/gitlab-org/gitlab/-/issues/521206 

✅. Helped Sec team Bonnie and Jocelyn with https://gitlab.com/gitlab-org/ux-research/-/issues/3368  - help draft dicussion guide + meeting about metrics

✅. Helped Sec team Alanna and Amar with https://gitlab.com/gitlab-org/ux-research/-/issues/3371

✅. Created a CAB proposal for Alanna and Amar with https://gitlab.com/gitlab-org/ux-research/-/issues/3405 

✅. Provided a summary of what we know already about Project Pelican -> https://gitlab.slack.com/archives/C08BX6N790B/p1740765268089609 

## Week of 2205-02-17 -OOO all week 

## Week of 2205-02-10
✅. wrote up notes for Participant 1 CAB follow-up interviews and provided some feedback for Libor 

✅. synthesized the [research questions from the AI related projects in the hopes of finding some efficiency](https://docs.google.com/document/d/1sG3e5wrCrLTQgKsmEGJ8OtMFdUDd63tbZKEzmws1PVE/edit?usp=sharing)

✅. provided feedback on the Hosted Runners for Dedicated interview discussion guide 

✅. Met with Sunjung about CI Steps research 

✅. Reviewed final report for GitHub SCM report 

✅. summarized some [research related findings to ongoing research on GitLab Updgrades, Operations and installation](https://gitlab.com/gitlab-org/ux-research/-/issues/3308#note_2337808142)

✅. Take steps to support conference registration for KubeCon EU for our 2 local research champions

## Week of 2025-02-03
✅. Meet with Pini to [map out when we'll deliver the Duo Workflow gaurdrails](https://docs.google.com/document/d/1Cl4DnWOXjmWzZllzPKaLTnzH8fnWlkSjODe6PtZZJ64/edit?usp=sharing)

✅. Help schedule the CAB follow-up interviews - https://gitlab.com/gitlab-org/ux-research/-/issues/3349 

✅. Provide feedback on the GitHub SCM study analysis / report from Gina

✅. Provided feedback on the [AI Security Risk Framework Blog and Whitepaper](https://gitlab.com/gitlab-com/marketing/brand-product-marketing/content-strategy-and-ops/blog/-/issues/296#note_2330797914) based on our AI and Compliance study 

✅. Drafted Discussion guide for Enterprise requirements for purchasing hosted runners - Linux for GitLab Dedicated (GA) and supported recruit

✅. Drafted a light survey for the beta for Pini to consider

## Week of 2025-01-27
✅. Iterated with Pedro on the UX Bash 2025-01-17 Duo Workflow Report 

✅. Published the AI and Compliance study 

✅.[ Drafted Screener for Duo Workflow AI Usability Metric Recruitment Screener](https://docs.google.com/document/d/1XP1LrB2W4uw7u-idKR4SG5cTvNoLkA--1lyYzyuz_kM/edit?usp=sharing)

✅.Started recruting internal participants for Duo Workflow research 

✅. Met with Pini and Pini, Pedro and Roger to go over Beta plans

✅. Drafted an epic for Q1/Q2 Duo Workflow research https://gitlab.com/groups/gitlab-org/-/epics/16619 

✅. Created a summary of [Duo Workflow - Use Case Success Stories 😎 -based on Dogfooding Survey](https://docs.google.com/document/d/1mBs0DSfmawizki_3UD_3M8VMq_FBGkd6a_BvZvD-CqU/edit?usp=sharing) to help us identify the tasks that we want to use for usability testing

✅.https://gitlab.com/gitlab-org/ux-research/-/issues/3364 worked out a plan of support with Sarah Derives from pricing to support this projects -- it's on our Q2 Product roadmap 

✅. GitHub CSM - met with Ruthvik and Gina to talk about analysis approach

✅. [Submitted Blog Proposal](https://gitlab.com/gitlab-com/marketing/brand-product-marketing/content-strategy-and-ops/blog/-/issues/303) for thought blog  on AI Agents Gaurdrails [Doc version](https://docs.google.com/document/d/1Mq-MI-hWob4U423WZdxD031rjXTsFH4AybbRlcrCw6o/edit?usp=sharing)

## Week of 2025-01-20 (MLK Day OOO 2025-01-20)
✅. Run CAB Session(s)

✅. Review Pedro's UX Bash findings

✅. [Created summary tables to summarize end-of-bash survey responses](https://docs.google.com/document/d/1KMqWEkt229nvG9w06s9wqUJ6poJXvnDeYqVakL9h-kg/edit?usp=sharing) 

✅. [Analysis for End-of-Bash Survey Results](https://docs.google.com/document/d/1KMqWEkt229nvG9w06s9wqUJ6poJXvnDeYqVakL9h-kg/edit?usp=sharing)

✅. CAB Summary - work with PMs (nothing too formal as we had n = 5 responses)

❌. Meet with Pini regarding WF positioning - instead focused on outlining an approach to gating access within a Project as part of the Beta

✅. Drafted an issue and Rally project to support follow-up interviews with up to 5 CAB participants

✅.  {Reach Goal} get feedback from UX Research team on the AI and Compliance Study report

✅. Worked with Nick and Ben to [draft of AI Usability Metric Revision Proposal (after consult with Jackie B that this was a good place to focus)](https://docs.google.com/document/d/1jmf1W4uuXNHCYh7WU8oZLxjm_NHo_0wGW1UPz86H9Bs/edit?usp=sharing)

✅. Drafted Discussion Guide and [Screener](https://gitlab.eu.qualtrics.com/survey-builder/SV_2n9FN3iQN6tv8Fg/edit?SurveyID=SV_2n9FN3iQN6tv8Fg) for [Internaly Usability study](https://docs.google.com/document/d/1-AicWcBdoI-w0uRfg2S3EP_Knw7CkYr6lDcWCdA5QuI/edit?usp=sharing) 

## Week of 2025-01-13
✅. (Contingent on when David will have time to provide feedback) Finalize CAB Discussion Guide, Deck and supporting materials and send to team so that they know where to take notes

✅.  [Customer Maturity Model Framework Workshop 2025-01-15](https://gitlab.com/gitlab-com/customer-success/digital-success/program-management/-/issues/3)

✅. Meet with Amanda Shen about local research champions and create issue or tag folks in an existing one 

✅.. Sync with Dov + Sunjung on CI Steps Early Adopters

✅. help with recruit for foundational study around GitHub SCM Integration 

✅.. Sync with Veethika and Ruthvik about GitHub SCM Integration 

✅. {Reach Goal} work on research report for the AI and Compliance Study

✅. Read draft and provided feedback on draft of [GitLab's AI Security Risk Framework](https://docs.google.com/document/d/1idVtmGInSqLLKb79SDkfXepA6VvTeO3yy5sv-UqeiYU/edit?usp=sharing) based on what we learned about what auditors are looking for in adoption decisions

✅. Created a [summary of what auditors are looking for in whitepapers / docs](https://gitlab.com/gitlab-com/marketing/developer-relations/developer-advocacy/developer-advocacy-meta/-/issues/637#note_2296875458) to help inform drafts for self-hosted models that our teams are drafting 

## Week of 2025-01-06
✅. Draft and refine CAB deck in concert with Haim (PM), Sacha (PM), Libor (Design) and maybe Gabe (PM)

✅. Access request to Qualtrics for Gabe and Sacha -> https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/33894 

✅. Hold CAB coordination meeting

✅. USAT sync meeting with Thaina 

✅. Support recruite for Sol Val https://gitlab.com/gitlab-org/ux-research/-/issues/3324  -- meet with Karen to coordinate support 

✅. Meet with Sunjung (Designer) + Dov (PM) to coordinate CI Steps Continuous Feedback Assignments 

✅. Provided feedback on discussion guide for https://gitlab.com/gitlab-org/ux-research/-/issues/3324 

✅. Provided some ideas for the Contributor success team who wants to get feedback on Duo - https://gitlab.com/gitlab-org/developer-relations/contributor-success/team-task/-/issues/621#note_2283729189 

✅. worked with Claude to draft a competitive landscape to use in the intro to our broader deck 

✅. Meet with Sunjung and Dov to support the CI Steps Continous Feedback Program 

✅. Monthly sync with Veethika

✅. Set up recruit for GitHub - SCM/CI study

✅. Created budget for local UX research champions

## Week of 2024-12-16
✅. finalized the draft of the core team alignment activity

✅. Met with Sunjung and Dov on the CI Steps continuous feedback program + created some general customer and internal feedback surveys for us to use https://gitlab.com/gitlab-org/ux-research/-/issues/3007#note_2263831398 + created some email templates for the team to use + fine tuned the intro screener

✅. (2nd meeting_ Met with Dov and Sunjung to support sending out recruitment emails this week 

✅. CAB planning meeting and prep notes / resources doc

✅. AI Strategic Alignment meeting

✅. Summarized the 2024 DORA report https://gitlab.com/gitlab-org/ux-research/-/issues/2549#note_2268398906 

✅. Complete Accessibility training 

✅. Redrafted the Competitive UX Analysis for AI Coding Assistants with Agentic Aspects issue - https://gitlab.com/gitlab-org/competitor-evaluations/-/issues/49# 

✅. Made a comment to officially pivot the SDLC golden journey analsysis out of JTBD land  https://gitlab.com/gitlab-org/ux-research/-/issues/3284#note_2268267617 + made a comment in the Plan issue that we don't need a high fidelity signal / can rely on previous research 

✅. USAT - reviewed discussion guide for the interviews - https://gitlab.com/gitlab-org/ux-research/-/issues/3320#note_2272303290 

## Week of 2024-12-09
✅. Meet with Suzanne Selhorn to socialize findings from AI and Compliance study and how we optimize the Duo Workflow (and Duo for that 
matter) information / "white papers" 

✅.. Ran a small aligment activity within the Duo Workflow team and as a next step drafted some talking points for the team to use - https://docs.google.com/document/d/10B8AyAuaH1MLACtkRStKKoAre3I8lmjmGqx18-gaEfE/edit?tab=t.0#heading=h.18i04vt7rv6z

✅. Reviewed qual findings for https://gitlab.com/gitlab-org/ux-research/-/issues/3302

✅. Drafted CAB Disucssion guide and provided supporting materials to Haim -> https://docs.google.com/document/d/1nynvFY9Cd4hDrcUuooqglyqKsEFHvSr6xwaYccNuz_I/edit?usp=sharing 

## Week of 2024-12-02 
✅. Met with Jackie to note that we likely have a need for both a Duo Workflow vision alignment along with a strategic AI Alignment project  - She'll check with Angela and circle back

✅. Drafted a quick survey to run by Pini next week related to team alignment

✅. Met with Nicolle and Pedro to sync on competitive analysis and started digging into [some of the tools in this doc](https://docs.google.com/document/d/1wi2qabF19_QrUyMNJ28WXrvv7Glzrgl9VXa8fnlgtHI/edit?tab=t.0) - suggested that we create a 1 page proposal to have Angela vet the approach

✅. Met briefly with Pedro about V: Conversational vs. Non-conv. UI < Duo Workflow https://gitlab.com/gitlab-org/ux-research/-/issues/3302#top + [ran a stats analysis for that comparison](https://gitlab.com/gitlab-org/ux-research/-/issues/3302#note_2242596838)

✅. Drafted different approaches for the Duo Workflow Alignment Interviews, survey, interview deck and more detailed follow-up discussions --> not sure though if this work will get depriotorized in favor of broader work on AI strategy. We should be able to use it at some point though. 

✅. IDP Async Roundtable -- got the panel ready for next steps by creating video montages and summary slides  - https://docs.google.com/presentation/d/15dq0Uo1MuvunBUBJRR3uBzWLvP4oiOGNNLv6EcLdICg/edit?usp=sharing  - working with stakeholders to get the information that we need to action / get clarity on how we might proceed

✅. Met with Afroditi Milidou, from legal, to socialize findings from AI and Compliance study and how we optimize the Duo Workflow information / "white papers"

✅. Brought up the importance of waiting for the UX Bash until we're confident that we'll see improvements in the Duo Workflow team meeting

✅. Provided feedback on discussion guide for [CI Solution validation: Immutable tags](https://gitlab.com/gitlab-org/ux-research/-/issues/3286) and assisting with recruit https://gitlab.com/gitlab-org/ux-research/-/issues/3287

## Week of 2024-11-25
✅. finalize UX local research champions survey for release next week + Link in issue + update timeline in issue

✅. Begin tagging IDP videos - Dovetail: https://gitlab.dovetail.com/projects/1z3GJbRv7KLXXfptAA1gJ7/v/5BuqdyhRVz0aLnw7stHLEv 

✅. Drafted an issue to track learnings from customer interviews - https://gitlab.com/gitlab-com/user-interviews/-/issues/33 + an activity to run within customer sessions 

✅. Provided feedback on Gina's Immutable Tags solution validation study - https://docs.google.com/document/d/1JdG3yGVQHO67uIjg4YY64EEFujUsU4uln_7_f6vJ0x4/edit?usp=sharing

## Week of 2024-11-18
✅. Meetings with stakeholders for research prioritization 

✅. Share out report [[Report Brief] Enterprise Leaders Guardrails and Expected Impact on Teams for Duo Workflow](https://docs.google.com/presentation/d/1nUNd4E4QjGtV-jbAh4CVyFOdohriKuMjmmgsg7eBVzE/edit?usp=sharing)

❌  finalize UX local research champions survey for release next week + Link in issue + update timeline in issue

✅. [Internal Developer Platform (IDP) Async Roundtable Discussion - “Backstage for Panel”](https://gitlab.com/gitlab-org/ux-research/-/issues/3282)

✅.. Provided feedback on https://gitlab.com/gitlab-org/ux-research/-/issues/3231 and the recruitment screener

## Week of 2024-11-11
✅. Meetings with stakeholders for research prioritization  - met with Pedro and Basti

✅. AI and Compliance Study Analysis -- where Agents should be able to take action -- created this issue to sync with team https://gitlab.com/gitlab-org/ux-research/-/issues/3267 

✅. [AI and Compliance study analysis -- write up the final list of guardrails and create an issue to start to socailize](https://gitlab.com/gitlab-org/ux-research/-/issues/3281) 

[AI and Complinace study analysis -- pulled out ways that AI/AI Agents can support compliance / auditing workflows](https://gitlab.com/gitlab-org/ux-research/-/issues/3162#note_2207180642)

✅. Drafted a budget for the UX Local Research Champions program to use in fiscal planning https://docs.google.com/document/d/1gK0f6Eljh74Ly8etmXvnV2-dBTDGLGQs044Rz9TEk5Y/edit?usp=sharing 

✅. Sync with Sean on pricing 

✅. Drafted an async UXDR North Star Duo Workflow participatory designs activity https://gitlab.com/gitlab-org/ux-research/-/issues/3283

## Week of 2024-11-04
❌ sync with Sean on pricing

✅. Provided feedback on the pricing interview guide -> https://docs.google.com/document/d/1mJhXOccto34KO3fJqc_lco3IEbJAGOe_/edit

✅. Run marathon 8 sessions for AI and Compliance study

✅. sync with Sunjung to scope out Q4 support for CI steps project

✅. Created a [use case summary report for all the entries in our internal dogfooding survey](https://docs.google.com/document/d/19Uj3pp3yoiBQbQ_xBYZAk6Fprug4hO-etWUaf4naE_A/edit?usp=sharing) + [used that to frame some different use cases that the team can iterate on in our new issue ](https://gitlab.com/gitlab-org/gitlab/-/issues/502874)

✅. Select new participant to replace the one who cancelled from the study

✅. Provided feedback on the first / [early draft of the pricing interview guide ](https://docs.google.com/document/d/1mJhXOccto34KO3fJqc_lco3IEbJAGOe_/edit)

## Week of 2024-10-28
❌ sync with Sean on pricing

✅. Run 3 pilot sessions for AI and Compliance study and iterate with Eng on the approach

✅.. Finalize Duo Workflow Compliance study discussion guide and interview deck based on learnings from pilot

✅. Met with Veethika, Ben and Jackie to talk about the walk through for the Create-CI-Sec journey

✅. Met with CI/CD Designers to look at TeaCity from a competitive lens

✅. Present on Enterprise Defining Success study in the Product Division Monthly --> https://docs.google.com/presentation/d/1k9fJoqgkPX2zaK2-yQKydc_Xl3W44u1GIZNLUjQqZ5k/edit?usp=sharing 

✅. Posted comment about guardrails and a call for research queestions for the compliance study in the current debate between Eng -> https://gitlab.com/gitlab-org/gitlab/-/issues/500780 

## Week of 2024-10-21
❌  sync with Sean on pricing -- he keeps pushing out our sync

✅. Draft Duo Workflow Compliance study discussion guide + iterate with Eng team + bring in Security Analysts for review 

✅. (reach goal) Draft a ~~plan~~ [survey to collect interest around UX conducting reseearch at conferences and share with team](https://docs.google.com/document/d/1h44KX0a9Gl-5ssG0S1H-hsx3QX_95Ey-mrKlHYAbo7g/edit?usp=sharing) for iterating - https://docs.google.com/spreadsheets/d/1Tk5fVSwpsEZVFbKOtuaPAItTpAE2PjPeyZ4jKoDe42w/edit?gid=571560493#gid=571560493 + Slack post - https://gitlab.slack.com/archives/C079R892ESZ/p1726511427978529 + issue: https://gitlab.com/gitlab-org/ux-research/-/issues/3252 

✅. Publish report on [[Report Brief] Enterprise Leaders Defining Success for AI - Duo Workflow Strategic Research](https://docs.google.com/presentation/d/1k9fJoqgkPX2zaK2-yQKydc_Xl3W44u1GIZNLUjQqZ5k/edit?usp=sharing)

✅. Finalze schedule / recruit for compliance study + send out background survey for participants recruited via Respondent

✅. Review and respond to Karen'ts C-SAT analysis 

✅. [Draft report brief on guardrails and impact to teams for Duo Workflow ](https://docs.google.com/presentation/d/1nUNd4E4QjGtV-jbAh4CVyFOdohriKuMjmmgsg7eBVzE/edit?usp=sharing) 

✅. post call for feedback and intake for new Q4 research issues  - https://gitlab.com/gitlab-org/ux-research/-/issues/3140#note_2179374541

✅. Created an issue for WHITE PAPERS on AI Impact and Dev Productivity https://gitlab.com/gitlab-org/ux-research/-/issues/3245

## Week of 2024-10-14
❌   Identify participants from the Enterprise Defining Success inteview who we can follow-up with about compliance  -- don't need this

✅. sync with Veethika

✅. Draft Duo Workflow Compliance study discussion guide 

✅. Consolidate research questions for Duo Workflow Compliance study

✅. Coffee Chat with Caitlin Steele -- talk informally about "faster product delivery" as the success indicator championed by enterprise decision makers for Duo Workflow

✅. Sync with Danika and Libor on "faster product delivery" as the success indicator championed by enterprise decision makers for Duo Workflow

✅. Select participants from URI study prospects

✅. Provide feedback on sampling strategy for gitlab#466083 https://gitlab.com/gitlab-org/ux-research/-/issues/3222#note_2157904489

✅. Met with Sarah DeVries, new pricing researcher

## Week of 2024-10-07
✅. Review pervious GitLab research on compliance  - provided summary in research issue

✅. Close out OOO issues -- https://gitlab.com/gitlab-org/ux-research/-/issues/3203  

✅. sync with Pedro

✅. Meet with new Duo Workflow Engineering Manager

✅. Draft research questions for Duo Workflow and Comopliance study and ask team to review

✅. Drafted report brief on rankings for signs of success for [Duo Workflow Enterprise Defining Success](https://docs.google.com/presentation/d/1k9fJoqgkPX2zaK2-yQKydc_Xl3W44u1GIZNLUjQqZ5k/edit?usp=sharing)

✅. Copmpleted the internal Pricing survey - Video here -> https://drive.google.com/file/d/1ZXy-yKQJMVYIBylMJ3RHrgiNvCO43VgM/view?usp=drive_link

## Week of 2024-09-16 (Erika OOO 2024-09-18 to 2024-10-04) 1. 
1. analysis for [Enterprise Defining Success Interviews - Duo Workflow](Research Issue: https://gitlab.com/gitlab-org/ux-research/-/issues/3134)

## Week of 2024-09-09
✅. Research Prioritization

✅. complete table for URI for Compliance study - https://docs.google.com/document/d/1_JJLs7b2W8QEiXYIfTKJFZo0GcEj1abBfmjOrT7NO6o/edit 

✅. Create OOO issue and tag stakeholders for review - https://gitlab.com/gitlab-org/ux-research/-/issues/3203 

✅. Signs of success analysis for [Enterprise Defining Success Interviews - Duo Workflow](Research Issue: https://gitlab.com/gitlab-org/ux-research/-/issues/3134)

✅. met with Ben, Nick and Nicolle about a shared (Bronze) research project to help us learn about MR related workflows, automation and context https://gitlab.com/gitlab-org/ux-research/-/issues/3201 + created a recruitment issue so that Ben can pick up with that more easily 

✅. (unplanned) [Duo Workflow Customer Satisfaction and Feedback Survey ](https://gitlab.com/gitlab-org/ux-research/-/issues/3198)- Drafted based on constructs championed by Enterprise decision makers during the defining success study

## Week of 2024-09-02
✅. One more session for [Enterprise Defining Success Interviews - Duo Workflow](Research Issue: https://gitlab.com/gitlab-org/ux-research/-/issues/3134)

✅. met with Anne about working together to support Sean's external pricing study 

✅. Drafted issue for October CAB and refined topic --> https://gitlab.com/gitlab-org/ux-research/-/issues/3188

✅. [unplanned work - 2 hours] Drafted [2 slides ](https://docs.google.com/presentation/d/1Yxz_xIOYsA899Bvt-N5tj4WeOcB8Brmvv9NC0wND60A/edit#slide=id.g2f7662a8121_0_56)for the UXR Partnership deck -- detailing case studies for Duo Workflow and adoptoin barriers research

✅. Created Actionable Insight to target increase in CSAT -> https://gitlab.com/gitlab-org/gitlab/-/issues/483930 

## Week of 2024-08-26 (Erika OOO 8-26 and 8-27)
✅. Compile research program for Duo Workflow Q3 and Q4  - put research issues into the research prioiritzation calculator -> https://gitlab.com/gitlab-org/ux-research/-/issues/3182 

✅. Run 4 sessions for [Enterprise Defining Success Interviews - Duo Workflow](Research Issue: https://gitlab.com/gitlab-org/ux-research/-/issues/3134) 

✅. Summarized findings so far for the first participants in the [Enterprise Defining Success Interviews - Duo Workflow](Research Issue: https://gitlab.com/gitlab-org/ux-research/-/issues/3134) [here](https://gitlab.com/gitlab-org/ux-research/-/issues/3134#note_2079771657)

✅. Provided feedback on Secrets Early Adopters Assignment 4  https://gitlab.com/gitlab-org/ux-research/-/issues/3160

✅.  Draft a reseach issue for Sean: https://gitlab.com/gitlab-org/ux-research/-/issues/3183 


## Week of 2024-08-19
✅.Publish [Report fix failing pipeline deep dive duo workflow study](https://docs.google.com/presentation/d/1ve88nw5RAQXwSjB4PC5QzHB7BRQp5hHs_Q4bH8wQHCg/edit?usp=sharing) 

✅.  Run ~4 sessions for [Defining Success Interviews - Duo Workflow](Research Issue: https://gitlab.com/gitlab-org/ux-research/-/issues/3134)

:x: Compile research program for Duo Workflow Q3 and Q4 --> Drafted but not completed, needed to wait for Pedro to get back from PTO

✅. Met and coordiante recruit for compliance auditors - meeting notes -> https://docs.google.com/document/d/1SLSLAJKoOxBtFTMilT0HqyySvrOZAFl3_NkX2nGvGgc/edit 
 
✅. created reserach issue for Duo Workflow and Compliance study -> https://gitlab.com/gitlab-org/ux-research/-/issues/3162 

✅. drafted a screener for [compliance auditors study recruitment](https://docs.google.com/document/d/1OVOabC94RMZREqBeb7u34XI6EeBbdMzk9gpxtOeGy84/edit?usp=sharing) and asked for review by our SecCompliance team

✅. Review Sean's research plan - [(2024) GitLab Pricing and Packaging Research Brief](https://docs.google.com/document/d/1kx6-lkUCYsU7TiW-KbWGL9XWFLT52DsQ1U9L5S9uuVQ/edit?usp=sharing) -- didn't dig into sampling, I defer to Sean on that

✅. Answered some of the Duo Workflow team's early research quetsions [in this issue](https://gitlab.com/gitlab-org/ux-research/-/issues/3076) (factoring the unanswered ones into Q3/Q$ planning)

✅. Met with Hannah S for office hours consult on Tokens internal survey -- created a Qualtrics template for her to use


## Week of 2024-08-12
✅. Try to help support recruit for Defining Success Interviews - Duo Workflow -- We're recruiting from 3 different sources - Rally, Respondent and CSMs 

✅. Run 2 sessions for [Defining Success Interviews - Duo Workflow](Research Issue: https://gitlab.com/gitlab-org/ux-research/-/issues/3134)

✅. Accrue and respond to feedback for [Report fix failing pipeline deep dive duo workflow study ](https://docs.google.com/presentation/d/1ve88nw5RAQXwSjB4PC5QzHB7BRQp5hHs_Q4bH8wQHCg/edit?usp=sharing) 

✅. Stakeholder planning conversations for Q3/Q4  - Verify, Package and Duo Workflow - wtih Pini, David, Taylor, Jacki B, and Jackie P

✅. Met with Nicolle Merril to sketch out a research plan for testing Duo context --> https://docs.google.com/document/d/19ehE2al16zKSvOSfMZZ9-bOGEhu5sSUI39_EVBfhvaw/edit?usp=sharing 

## Week of 2024-08-12
1. [Reach Goal] Analysis and early draft of report for 2 security related workflow prompts -> for Sec team and next dedicated focus for Duo Workflow -> https://docs.google.com/presentation/d/1mz134opMjvNQfVnIj0IdflXF3WeH31-elIV8i16eMck/edit?usp=sharing -- only complete the updated depdencey report

## Week of 2024-08-05 
 ✅.  work with Jocelyn to write up learnings from Secrets CAB activity - quick doc [report brief](https://docs.google.com/document/d/1HK4OxvEtY28B7vW2UgoUFkYIo-JSpRgb9lgpUmmfxVw/edit?usp=sharing)

 ✅. [analysis for fix failing pipeline deep dive duo workflow study ](https://docs.google.com/presentation/d/1ve88nw5RAQXwSjB4PC5QzHB7BRQp5hHs_Q4bH8wQHCg/edit?usp=sharing)

 ✅. [release [Report] Lessons from DevOps Portals / Internal Developer Platforms ](https://docs.google.com/presentation/d/1VtdotLh0p1sEGawadO98DqWwkNPsiHgY-WnJZvg49qM/edit?usp=sharing)

 ✅. automate depdency deep dive sessions, only 4 to date bc of a few cancelations

 ✅. Defining Success Duo Workflow interview recruit - sent out reminders to CAB folks, worked with Caitlin to invite 9 decision makers from the 2022 intergrations survey, posted in the CSM coordination issue and the Ai-field slack channel 

## Week of 2024-07-29 

✅. analysis and report for Duo Workflow Deep Dive interivews - fix pipelines 

✅.. Meet with Veethika and Pedro to create plan for analysis of fix a failed pipeline study

✅. Analysis and draft report for fix pipeline deep dive 

✅. [Reach Goal] Analysis and early draft of report for automate depedency workflow prompts in prep for Deep Dives

✅. Adjust Deep Dive Interview Discussion Guide for Automate Dependeny Updates

✅. send out screener to CAB members for Defining success Interviews + created a back-up plan bc of low response rate

## Week of 2024-07-22
✅. Met with Pini

✅. [Analysis and draft of report for early concept interview fixing pipelines and fix failing tests](https://docs.google.com/presentation/d/1xvyYTcFGAi8VX_W8_N60ogndjOBbel_Sm_jC2TA5ohA/edit?usp=sharing)

✅. Wrote up Analysis for Deep Dive on fix Failing Pipeline Duo Workflow  - wrote up gaurdrails, 5 signs of success, user goals and sub-goals

✅. Decide whether to run 5 participants in a automate dependency updates study 

✅. Created issue, discussion guides and interview deck template with Enterprise "Defining Success" interviews

✅. Met with Sean to review plan for Enterprise "Defining Success" interviews

## Week of 2024-07-15
✅. [Duo Workflow report brief related to feedback on the early concept ](https://docs.google.com/presentation/d/1Z7vuGBe0ncG7W5YcF1Ayj_xSMEjfRZeRLIDJ7KVvhW4/edit?usp=sharing)

✅. Create video and release [report on [Report] Duo Workflow Early Concept Interviews - Report Brief: Which use cases would we be the most helpful to have streamlined by AI?](https://docs.google.com/presentation/d/1vgUhGCv2XZqcV_h4ZcJiAi3_n7wzexvqDTRi9nkwPHs/edit?usp=sharing)

✅.  Run Duo Workflow Deep Dive Interviews for Fixing Pipelines & Fix failing tests

✅. Draft dogofooding feedback survey with UX Research team and Duo Workflow team  - https://gitlab.com/gitlab-org/ux-research/-/issues/3113


## Week of 2024-07-09 (OOO on Monday 2024-07-08)

✅  Meet with Sean to talk about Duo Workflow research needs - pricing - what the plan to outsource that work is and what support is needed

✅  Meet with Pini to confirm next steps for Duo Workflow research 

✅ Get feedback on and finalize 3 Duo Workflow report briefs drafted -> (1) [Overview report](https://docs.google.com/presentation/d/1vgUhGCv2XZqcV_h4ZcJiAi3_n7wzexvqDTRi9nkwPHs/edit#slide=id.g2c7a6bd028b_0_23), (2) [Fix failing Pipelines Report](https://docs.google.com/presentation/d/1xvyYTcFGAi8VX_W8_N60ogndjOBbel_Sm_jC2TA5ohA/edit?usp=sharing), (3) [automate dependency updates](https://docs.google.com/presentation/d/1eqhsI4JJJPrws3_2KPD6_Juj0S-shdlAHN9qPpNoNUo/edit#slide=id.g2b14eae506f_0_0)

✅ Draft a dogfooding feedback survey with questions that we can narrow down for use with external customers -> https://gitlab.com/gitlab-org/ux-research/-/issues/3113

✅ Run Duo Workflow Deep Dive Interviews for Fixing Pipelines 

✅ Recruit for Duo Workflow Deep Dive Interviews for Fixing Failed Tests

✅  UX Research vision team work 


## Week of 2024-07-01 (OOO on 2024-07-04 + 2024-07-05)
✅ CAB Research Activities

✅ [Report Brief: Summary of Customer Advisory Board (CAB) Feedback on Duo Workflow [Early Concept] ](https://docs.google.com/document/d/1wJSw4GK6duxx1X5swEmKAOZO2idS4UCafWYv1362RPQ/edit?usp=sharing)

✅ [Draft [Report Brief] Duo Worklfow Early Concept Interviews - Fix a failing Pipelines, falling tests, and linting](https://docs.google.com/presentation/d/1xvyYTcFGAi8VX_W8_N60ogndjOBbel_Sm_jC2TA5ohA/edit?usp=sharing)

✅ [Draft [Report Brief] Duo Worklfow Early Concept Interviews - Automate Dependency Updates & Create and Update Container Images](https://docs.google.com/presentation/d/1eqhsI4JJJPrws3_2KPD6_Juj0S-shdlAHN9qPpNoNUo/edit?usp=sharing)

✅ [DRAFT [Report] Duo Worklfow Early Concept Interviews](https://docs.google.com/presentation/d/1vgUhGCv2XZqcV_h4ZcJiAi3_n7wzexvqDTRi9nkwPHs/edit?usp=sharing)

✅ Set up Deep Dive issue for Fix a failing pipeline / fix a failing job 

## Week of 2024-06-24
✅ Run CAB Secrets practice sessions 

✅ CAB Session Prep Deck and Practice Speaking parts

✅ https://gitlab.com/gitlab-com/legal-and-compliance/-/issues/2155 Issue to clear sharing report on Exploring the DevOps Loop with CAB

✅ Ran ~11 Early Concepts Duo Workflow study sessions

✅. CAB Duo Workflow Survey 

✅ Support Recruit for RCA Duo Feature via the Adoption Barriers Follow-up Interview

✅ Draft screener for Early Access program + create research issue to track need for support


## Week of 2024-06-17
✅ Meet with Taylor to talk about Duo Workflow research needs

✅ Meet with Sean to talk about Duo Workflow research needs - pricing

✅ Run ~8 Early Concepts Duo Workflow study sessions

✅ Recruit more Early Concepts Duo Workflow participants

## Week of 2024-06-10
**AI**

✅ Ran 6 Early Concepts Duo Workflow study sessions

✅ Analysis for each session and summary for the team 

✅ Met with David to talk about Duo Workflow research needs

✅ Met with Pini to talk about Duo Workflow research needs

✅ Drafted [Duo Workflow: Who is the critical user?  Game - An adaptation of Rock-Paper-Scissors game](https://docs.google.com/document/d/13fE9TcNWCJzEaArs80ZRSLuxdW65iqbG6U7HKMVuPYg/edit?usp=sharing) for team to play to explore our target Personas 

**Non-AI**

✅ Review and provide feedback on Solution Validation for Dependency Firewall Discussion Guide - https://gitlab.com/gitlab-org/ux-research/-/issues/3051#note_1943512265

✅ Review and respond to team framing for Early Adopters Secrets Assignment 4

✅ Worked on Report for DevOps Portals / IDPs

✅ Draft [Discussion Guide for Duo Workflow - Deep Dive Interviews](https://docs.google.com/document/d/1y-QCItm_MEJAEgYeYJgooBeAf7zX2pXYU9mgqpLPxZE/edit?usp=sharing) + Issue + Recruitment Issue


## Week of 2024-06-03
✅. Run DevOps Portal Sessions and Process gratuity 

✅. DevOps Portal Analysis

✅ Met with Pedro to talk about Duo Workflow research needs

✅ Create research issue and recruitment issue for AI Early Concepts Duo Workflow study

✅. Attend Autonomous Agents team meeting
 

## Week of 2024-05-27 (000 May 27)
✅  Run DevOps Portal Sessions and Process gratuity 

✅  Work with Bonnie to finalize designs for the CAB activity

✅  Draft slides for CAB activity

✅  Meet wuth Bonnie and Jocelyn about CAB activity and post a summary of approach 

✅  Draft slides for Research Update for CAB

✅  Met with new Eng, Kamil, about mental models research

✅  Met with Gina about bringing in some Mental Models resarch tasks into her study and adjusting her study to cover those topics (though in less depth)

✅  Met with Caroline and Rutvik to review personas for RCA Duo UX study, overall approach, and timeline 

## Week of 2024-05-20 (OOO May 20 + May 21)
✅  Record an intro to Adoption barriers Dashboard video + set up socialization issue + publish that issue

✅  Recruit for DevOps Portal Research

✅  DevOps Portal Research Sessions 

✅  Meet with Adam about Q2 Prioritization 

✅  Actionable Insights for Adoption Barriers research

✅ Met with team about mental models research + had to bring in a new Eng

✅ Check-in with Jackie about research priorities


## Week of 2024-05-13
✅ meet with William Arias regarding Adoption Barriers research and/or set up time to do so + created an issue to share his work, I created a summary and tagged in folks to help support the creation of documentation -> https://gitlab.com/gitlab-com/marketing/developer-relations/developer-advocacy/developer-advocacy-meta/-/issues/425 

✅ Met with Jocelyn and Bonnie about two options for our secrets solution + drafted a new CAB activity to help us get clarity theree

❌ Draft or start iterating on Actionable Insights for Adoption Barriers research

✅ Follow-up with Viktor in Slack to get contact info for Backstage.io user

✅ Start manual recruit for CI Steps work - Reached out to 22 Account Managers with Slack + an email that they could forward to customers -> no sign-ups yet 

✅ Work to finalize CI STeps and Components study, set target dates for running sessions + another iteration on the discussion guide

✅ Recruit for DevOps Portal Research and promoted those sessions in Slack

✅ Finalized Discussion Guide and Interview Template for DevOps Portal interviews

✅ Drafted Issue framing for the Adoption Barriers Dashboard

✅ Met with Gina to parse our the mental model study from her customer focused interview with high numbers of pipelines 


## Week of 2024-05-06
Y. Publish self-esrve spreadsheet so that PMs can schedule adoption barreirs follow-up interviews

Y. set-up practice session(s) for CAB Secrets activity

Y. Group PLT sticky notes into themes

Y. Meet with Dave P to walk through Adoption Barriers Dashboard + draft a report / frame the issue (if needed)

Y. Work with Engineers to draft new study plan for CI Steps and Components ->https://docs.google.com/document/d/1jr2DconVpwllVpUpcLNQFwaA6l5Eox08abHWC7O8WYA/edit

Y. Finalized draft of adoptoin barriers interview discussion guide 

Y. Meet with Danika to create a game-approach to present findings at Product meeting 

## Week of 2024-04-29
Y. Adoption Barriers PLT Session

Y. [Adoption Barriers Followup Interviews Discusison Guide](https://docs.google.com/document/d/1VSFP3j4QZG_chy_f5lPY3GhIoAxU-SI9er4lN-tNYaI/edit?usp=sharing)

Y. Help articulate requriements for designs for the CAB activity related to Secrets

Y. Scope out the CI Steps and CI Components study and meet wtih Eng to get more background -> UX Study: https://gitlab.com/gitlab-org/ux-research/-/issues/3009

Y. Create an early adopters program for CI Steps -> https://gitlab.com/gitlab-org/ux-research/-/issues/3007

Y. Meet with Ian Kohr, Compliance PM, to discuss research / have him shadow my process

Y. Drafted an issue to look into CI Mins and why those are a high severity challenge -https://gitlab.com/gitlab-org/ux-research/-/issues/3009 

Y Respond to UX Research team review of the DevOps Portal Screener

Y. Drafted Discussion Guide for DevOps Portal Screener

Y. Draft FY24 Q3/Q4 Study2: Verify Stage Usability Benchmark ->  https://gitlab.com/groups/gitlab-org/-/epics/13674

Y. Met with Martin, strategic CSM to talk about adoption barriers study + CI steps

Y. Met with Sarah Walder (via Slack while she was in the airport) to introduce her to the adoption barriers research and approach to follow-up interviews

Y. Connect PMs to CSMs for 14 Top ARR customers so that they can schedule follow-up inteviews with that group -> https://gitlab.com/gitlab-org/ux-research/-/issues/2912

Y. Coordinated Lindsy Farina, PM for Product Analytics, visiting UX Research team meeting

Y. Met with Nicole CI/CD Product Analyst to talk about CI Compute Mins and why those might be a high severity challenge

Y. Create an issue to support the creation of a benchmark-like "environment" for compairing CI Steps, Components and Templates -> https://gitlab.com/gitlab-org/ux-research/-/issues/3008

### Week of 2024-04-22
 Y. AMA with UX Research: Secrets

 Y. JTBD Secrets feedback and sync session if needed

 Y. Talk about prospective PLT Offsite session related to adoption barriers

 Y. Q2 Research Prioritization

 Y. Meet with Jackie to get research questions

 Y. Check in on June CAB plans

 Y. Created issue for DevOps Portal Research and start recruitment issue + pulled potential contacts from Verify H2 Common Screener -> https://gitlab.com/gitlab-org/ux-research/-/issues/2994 

 Y. Drafted a 1 page plan for Duo Adoption UX Research Program  - https://docs.google.com/document/d/1q7U4mi-JCJmO2F6qtzYgoNuK2Xfd5zdEmuc7nVFHFtc/edit?usp=sharing

### Week of 2024-04-15 (OOO 2024-04-71 to 2024-04-22)
 Y. Adoption Barriers Product Presentation

 Y. [Adoption Barriers AMA - Cross-functional](https://docs.google.com/document/d/15lkpssBokYrnaGCE_rX_K9SUQFo_95G1R0LpXSUNCE4/edit?usp=sharing)
 
 Y. JTBD Secrets study Meeting
 
 Y. Assist with Secrets Early Adopters Assignment 3 
  
### Weeks of 2024-03-17 to 2024-04-08
 Y. Deep sprint on severity analysis

 Y. Severity analysis reliabiltiy coding

 Y. Adoption Barriers Report

 Y. Meetings with Adoption Barriers Key stakeholders
 
 Y. GitLab Secrets Manager - Early Adopters - Assignment 3 

### Week of 2024-03-10
 - GitLab Summit

### Week of 2024-03-04
Y. Finalize Adoption Barriers presentation for the Summit Product All Hands 

Y. CAB meeting - prep and participation

Y. Meet with Danika to go over next steps for the Closing the DevOps loop study + CAB report


### Week of 2024-02-26
Y. Met with Mike about CSM survey 

Y. Support roll out of CSM-led survey

Y. CAB practice session 2024-02-28

Y.~~ Create Papercuts Issue for SUS and Adoption Barriers~~ -> Nick will fold this into the existing SUS process

Y. Drafted Adoption Barriers presentation for the Summit Product All Hands 

Y. Met with Bonnie on her upcoming solution validation study

Y. Provided feedback to Sunjung on her study on CI components

### Week of 2024-02-19 - OOO 

### Weeks of 2024-01-29, 2024-02-05, and 2024-02-12
Y. Conducted SUS analysis and created a [SUS Verbatim Coding Companion Guide to support Nick ](https://docs.google.com/document/d/18bpS22SecLuCktUeDAMuCGQmH6GjA08BbGcof_EJvyg/edit?usp=sharing)

Y. Ran 26 60 min adoption barrier interviews

Y. Drafted overall adoption barriers and survey items based on adoption barrier interviews

Y. Built out survey in Qualtrics 

Y. Got feedback from cross-stage stakeholders and adjusted survey line items

Y. Ran 4 survey development interivews

Y. Coordinate CAB approach and practice session 

Y. Completed AI Diary analysis and report 

Y. Helped Bonnie update her recruitment screener 

### Week of 2024-01-22
Y. meet with Darren + CSM on key Red account background

Y. met with Mike F to give overview of study and get feedback

Y. run CI Yellow and CI Secure interviews

Y. draft Survey development doc for CI and Secure adoption
 
Y. Create survey(s) to support a CSM + user approach to recruit large enterprise into yellow surveys
 
Y Create working doc for notes and sampling tables CI and Secure adoption barrriers study

Y. Attend CI Direction ThnkBig

Y. meet with Bonnie regarding research needs and solution validation

### Week of 2024-01-15
Y. AI Diary Follow-up Interviews n = 3

Y. AI Diary Ops Analysis + met with Anne and Nick on next steps

Y. Try to unblock the GitLab Adoption Barrier study -> worked with 3 different teams to find and adjust the rest of the list

Y. finalize CAB date and posted research questions draft and date of meeting to team 

Y. support Darren with setting up issue for Key red accounts inteview + iterated on discussion guide + help to keep conversation with CSM contacts going

Y. met with Micheal + Dave about the project and worked with him to get a sense of overlap in CI and Secure users -> Secure users are a subset of CI users

Y. Schedule sessions for CI and Secure adoption barrriers study 

### Week of 2024-01-08
Y. Prep for Diary Interviews

Y. AI Diary Follow-up Interviews n = 5

Y. Meet with Mike N about workshop

Y. ThinkBig for Pipeline Authoring

Y. sync with Danika on Next steps for Exploring the DevOps Loop

Y. Met with Nick and Will about QuantCon Paper - Nick will solo author (no time for this)

### Week of 2024-01-01
N. ~~Review team article outline by Nick~~

Y. Adjust [teams workshop](https://gitlab.com/gitlab-org/ux-research/-/issues/2766) so that it's after the summit (2 weeks as designers will be on PTO around it)

N. Create and send sreener to embedded computing contacts => moving to list of backlog tasks

Y. Provide [feedback on Google and GitLab Project Integration and Setup](https://docs.google.com/document/d/1Rzqwh6lAx2WsfGUXExL_Gv7GZk_-vto-4L2dUCkm1jY/edit?usp=sharing) 

Y. Create research issue for Q2 Adoption Barrier Cohort Study (Prioiritized on 2024-02-14)

Y. Draft Q2 Survey Development Interviews issue  (Prioiritized on 2024-02-14)

Y. Prep for Diary Interviews

### Week of 2023-12-18
N. Postponing until after summit -> Add teams workshop to Summit events list

Y. Completed AI Diary Interviews

Y. [AI Diary - Interview 1 Analysis](https://docs.google.com/document/d/1IidngS187dqBNB7hZ6woR5h0XZ38VLCeDv-VF3PpYEY/edit?usp=sharing)

Y. Draft [Interview Deck for follow-up Convo AI Diary Study](https://docs.google.com/presentation/d/1vs8RLiW7hy0WR6LAVktMqX5fXquYl5rirQDpoKPXC4c/edit#slide=id.g2646c2268ec_0_0)

Y. Draft CAB materials + February Issue

Y. [Draft Adoption Barrier Study Issue ](https://gitlab.com/gitlab-org/ux-research/-/issues/2840)

Y. Draft [Request for Data and Insights team to create an ML model to predict probability of CI Readiness ](https://gitlab.com/gitlab-data/analytics/-/issues/19315)

Y. Met with CI Build and Runner team to disucss Kano findings 

Y. Kano Qual analysis 

### Week of 2023-12-11
Y. CAB activity summary

Y. Schedule follow-ups with AI Diary participants

Y. Create AI Diary Interview Deck Template

Y. Ran 8 AI Diary Interviews

Y. Gave feedback on UX testing for merging experience - group interview format: https://gitlab.com/gitlab-org/ux-research/-/issues/2764#note_1694966435 

Y. Gave feedback on issue post for public feedback on merging experience 

Y. Met with Bonnie, new Pipeline Security Designer

Y. Runner and CI Builds Feature Prioiritization Survey Analysis + Report -> https://docs.google.com/presentation/d/1QAO0m08Om15AC2W0pZp0cfxCImgmww-qvwyKtlMQIBk/edit?usp=sharing 


### Week of 2023-12-04
Y. Meet with Jackie to sync

Y. Intro meeting with Rutvik Shah, new Pipeline Execution PM

Y. Created CAB Issue:  https://gitlab.com/gitlab-org/ux-research/-/issues/2808

Y. We made a pivot for the GCP integration to use CI steps so that users can employ 3 lines of code to address the anticipated adoption barriers that we anticipated based on survey results.

Y. Exploring the DevOps loop - respond to team feedback

Y. Exploring the DevOps loop - share report for review by UX Research team

Y. Feedback on discussion guide for https://gitlab.com/gitlab-org/ux-research/-/issues/2810 

Y. Monitor responses to Runner Feature Prioiritization Survey

Y. Help with discussion guide and recruit for CI Steps Study https://gitlab.com/gitlab-org/ux-research/-/issues/2814 

Y. Help with study plan for https://gitlab.com/gitlab-org/ux-research/-/issues/2772  

Y. Crate a UX Research Radar for Findings related to "Teams" -> https://gitlab.com/gitlab-org/ux-research/-/issues/2820

### Week of 2023-11-27
Y. Exploring the DevOps loop analysis

Y. Draft CAB Proposal for Jan/Feb 2024

Y. Process Gratuity for Runner Feature Prioiritization Survey

Y. Monitor responses to Runner Feature Prioiritization Survey

### Week of 2023-11-20 OOO 🦃

### Week of 2023-11-13 (Covid vaccine on 2023-11-16)
Y. team article respond to comments + meet with Legal

Y. social media posts for Runner Feature Prioritization

Y. observe Exploring the DevOps loop sessions + start on analysis

Y. attend cross-functional sync on secrets Management

Y. sync with Anne on AI metrics + Diary study

Y. monthly sync with Veethika

Y. sync with Sunjung on soution validation study

Y. sync with Martin about embedded development study screener

### Week of 2023-11-06 (OOO 2023-11-10)
Y. Exploring the DevOps loop sessions - 12 sessions

Y. gratuity for Pedro's first click Study

Y. support KubeCon NA data collection 

### Week of 2023-10-30
Y. Draft Exploring the DevOps loop study plan and interview deck

Y. Update research prioiritization issue

Y. Create issue to coordinate the Q1 workshop on shared resources - https://gitlab.com/gitlab-org/ux-research/-/issues/2766

Y. send out invites for Pedro's first click study

Y. check the responses for the Runners Kano survey + create a social media request to help us get more responses (we need 30 more)

### Week of 2023-10-23
Y. Provided feedback on Pedro's first click test for GCP and attended GCP integration meetings

Y. work on team paper 

Y. Create Q4 research prioritization issue 

Y. Report brief on tools, tasks and opportunities 

Y. Draft [screener](https://docs.google.com/document/d/1Oqw5pTLOtUE9Pphl0U2N7QmPmP-NVHR70u0mHqG7u4M/edit?usp=sharing) for Martin / Emdded Development interviews 

### Week of 2023-10-16
Y. Provided feedback on Pedro's first click test for GCP and attended GCP integration meetings

Y. Publish GitLab and the Cloud User Access and Shared Resources Report

Y. Set up issue for Bashing CI Components

y. Analysis for Report brief on tools, tasks and opportunities 

### Week of 2023-10-09
Y. GitLab and the Cloud User Access and Shared Resources Analysis

N. Refactor Research Prioritization Issue

Y. Followup with Marketing folks about KubeCon NA

Y. meet with GCP UXRs about GCP Integration research 

### Week of 2023-10-02
Y. GitLab and the Cloud sessions - 
y. analysis and draft report brief for research questions related to GCP integration
Y. GCP integration UXR update

### Week of 2023-09-25

Y. Create Kano promo game and set up timeline for project

Y. Create research issue for Martin interviews

Y. Draft Discussion Guide for embedded development interviews

Y. run GitLab and the Cloud sessions

Y. GitLab and GCP Artifact Management Early Adopter program - https://gitlab.com/gitlab-com/alliances/google/console-integration/-/issues/14	


### Week of 2023-09-18
N. Monthly sync with Veethika 

Y. fine tune interview script and ran 7 GitLab and the Cloud study sessions

Y. get clarity with new PM on Kano approach 

Y. announce AI metrics in Product meeting 

### Week of 2023-09-11

Y. Run up to 3 more AI Runner optimization study sessions + drafted the report

Y. Meet with Martin about https://gitlab.com/gitlab-org/ux-research/-/issues/2675 

Y. Release report on Data Collection Comparison - Email campaign vs MeasuringU 

N. Sync with Jackie 

Y. Finalized AI Metrics with Anne + announced in UX meeting

Y. send a follow-up ping about KubeCon NA data collection


### Week of 2023-09-04
Y. Ran 6 AI Runner optimization study sessions

Y. insert feedback into SUS papercuts issue

Y. Ran pilot for "GitLab and the cloud study" to see how close we are to being ready to run those sessions 

Y. Process Runner Terminology Survey Promo Game

Y. Meet with Cait about the Common screener and Rally

Y. Meet with Anne to go over feedback from running AI Metrics through UserTesting + created a Q4 issue to follow-up once we have more responses 

Y. Worked to refine feature descriptions for Runner Kano survey

Y. Provided feedback on Will's foundational study on Organizations and suggested research questions that we need to move the "Team" converstation forward

Y. Spoke with Ashley about using Cronbachs alpah to inform refinements in Nav survey question battery

### Week of 2023-08-28
Y. Move team paper along and draft sections

Y. Feedback on list of runner features for Kano -> finalized the list for the team, put it in qualtrics, got feedback from Eng, suggested changes, and started getting user feedback

Y. Draft AI Runner optimization interview deck + ran one pilot session

Y. publish GitHub vs GitLab SUS 

Y. Get UXR feedback on AI Metrics and meet with Anne to fine-tune based on feedback + put the AI metrics and the background questions into the AI UX Survey in Qualtrics 

Y. Provided feedback on sol val study for ci_job_token research 

Y. Met with team to create a plan for Veethika's departure / continuing research + early adopters program for Secrets

Y. Met with Mike N, Will and Ashley -> want to get 1 question on user roles and access into the Nav survey -> https://docs.google.com/document/d/1I1X7VmK2SiQxCT8GdIwioWpjKKe-1w2O8_TR2j3iObM/edit?usp=sharing 

### Week of 2023-8-21
Y. Assign daily estimates to Q3 prioritization

Y. Meet with Jackie to prioritize Q3 projects 

Y. Meet with Darren to talk about AI Cost optimizatin project for Runners

Y. send reminder emails about papercut issues for SUS outreach

Y. Set up Assignment 2 survey to reflect new task flow

Y. Draft issue for Kano survey, set up Survey template 

### Week of 2023-08-14
Y. check in on KubeCon NA data collection! 

Y. draft a GitLab and Cloud products workflow study (for Jackie review) - [Draft] How are users working with GitLab CI/CD and “the Cloud”? -> https://gitlab.com/gitlab-org/ux-research/-/issues/2633 

Y. process new requests to research and prioiritzation issue

Y. Drafted report for secrets assignment 1 -> 

Y. Met with Veethika to outline next steps for assignment 2 

Y. Met with Gina about and created a research issue for Runner + AI cost estimation -> How do we use AI to optimize cost and performance trade-offs in allocation of runners? -https://gitlab.com/gitlab-org/ux-research/-/issues/2630 


### Week of 2023-08-07 (OOO until 2023-08-09)
Y. NPS verbatim analysis - https://gitlab.com/gitlab-org/ux-research/-/issues/2597 

Y. Drafted a User Roles and Access Study -> https://docs.google.com/document/d/18DuCdLLrAXgaH_qrLT4uKZneYaA-wkZ5MKyy9W44xrk/edit?usp=sharing 

Y. Finalized slides for [SUS report](https://docs.google.com/presentation/d/1Dkq2cueFHJIxQQkalk-aYM40Vh27OeWjfzmfdRmerO8/edit#slide=id.g224a7329a5c_0_400)

Y. Met with Tim about https://gitlab.com/gitlab-org/ux-research/-/issues/2602 + found 30 prospects for him via Common Screeners + reviewed his discusion guide + drafted a back-up screener in case we need it (and shared with team for review)

Y. Adjusted the benchmark common screener so that my team can leverage it for cross-cloud projects + made a comment to track changes in case we need to revert them 1ater 

Y. Created papercuts Q2 issue, posted comments and sent follow-up emails (if available)


### Week of 2023-07-31
 - OOO until 2023-08-02
 Y. wrap up AI workshops
 Y. Complete SUS Analysis for GitLab and GitHub + drafted slides for reports

### Week of 2023-07-24
Y. move along AI workshop next steps

Y. feedback on Runner Mental Models research 

Y. Promo game and recruitment set up for next step study in Runner Mental Models research 

~~1. promo game for auto industry survey~~m -> Caitling says that we need input from product to do target that industry

Y. prompt team to post research studies to Q3 prioritization issue

Y. check in on the SUS verbatim cross-functional workflow link issues + look at MR for handbook

Y. met with Sunjung to talk about meeting about sideNav and next steps (I feel in the dark about this)

### Week of 2023-07-10
Y. move along AI workshop next steps

Y. Helped Gina with analysis on Runner Mental Models research + drafted a report starter deck for her to fill in

Y. Reach Goal: Draft Approach 2 to AI UX Metrics and meet with Anne to sync on approach (she prefers Approach 2)

Y. check in on SUS data (may begin the analysis if we have the data ready)


### Week of 2023-07-03
Y. Create research issue for analyzing GitHub verbatim -> https://gitlab.com/gitlab-org/ux-research/-/issues/2546 

Y. Draft and share [Q3 research prioritization issue](https://gitlab.com/gitlab-org/ux-research/-/issues/2548)

Y. Readjust timeline for Ops for All team AI workshops 

Y. Meet with Anna regarding AI UX metrics


### Week of 2023-06-26
N. [Blocked on this - working to create a new schedule to give us more time] Run 3 AI workshops with Ops for All team

Y. prep next steps for Verify Workshops with broader team - created an instructional video to help guide team discussion

Y. Updated [AI metrics issue](https://gitlab.com/gitlab-org/ux-research/-/issues/2537) to include more than AI Fit metric and drafted metrics for [7 AI UX constructs](https://docs.google.com/document/d/1luPEeirnbIpa3UUjQAeEJjvkN8vaKAFv-_Oh-Mm00Ak/edit#bookmark=id.za2l1e6k4c27)

Y. {I'll put this on hold now until after the holidays / PTO: Mid August} keep convo in cross-functional SUS opporunity issues going

Y. Assist with Mental Model Runners Research -> https://gitlab.com/gitlab-org/ux-research/-/issues/2444#note_1440637094

Y. Created final group of Early Adopter for Native Secrets + set up automatic reports for some assignemtns

Y. Meeting with tech writer on next steps for benchmark finding -> led to creation of proposal: https://gitlab.com/gitlab-org/gitlab/-/issues/416708 

Y. Consulted with CI usage survey -> https://gitlab.com/gitlab-com/sales-team/wg-automotive-development/-/issues/1#note_1411393359


### Week of 2023-06-19 (public holiday 6/19)
Y. Work with Gina to support runners mental model research - https://gitlab.com/gitlab-org/ux-research/-/issues/2539

N. watch more QuantCon sessions

Y. kickoff AI User Needs & AI Fit workshop

y. create plan for when to take next steps (based on time zones) for the 2 AI workshops the week of 2023-06-23 -> set up calendar event so team has transparency. I'll work on next steps from 5-7pm. 

Y. support Dan MH in his survey verbatim analysis


### Week of 2023-06-12
Y. write up learnings from Early Adopters for Native Secrets solution - intro screener - [Report](https://docs.google.com/presentation/d/1DQGpxJ69lBCEU0acnyNBTpCVrr0iHiV0CUS5nxPNIGE/edit?usp=sharing)

Y. MR to add a quick description of the steps that we're piloting to the product processes handbook: gitlab-com/www-gitlab-com!125913

Y. create issues for cros-functional SUS workflow Links - [gitlab#415281](https://gitlab.com/gitlab-org/gitlab/-/issues/415281) + [gitlab#415282](https://gitlab.com/gitlab-org/gitlab/-/issues/415282)

Y. finish creating issues and finalizing murals for AI workshops (onboarding paragraph + errors audit)

Y. attend UX QuantCon - [notes on sesssions](https://docs.google.com/document/d/1af__wn26IgCXQ9Xa9R6_Qx58HMZnW5NohEbxfyYQdIs/edit?usp=sharing)

Y. Provided support for survey themes analysis from Dan MH


### Week of 2023-06-05

Y. [Gave a lightining talk for Environment Design Sprint](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/18#lightning-talks-1-hour) - to summarize research on CI/CD

Y. Sync with Anne on AI research 

Y. Finalize CI Early Adopters report -> https://docs.google.com/presentation/d/1fUnKGbAAH8WbDVOW0zQjWhTNJp2psO4nPaIebGAenAE/edit?usp=sharing 

Y. Revise AI workshops based on feedback from UXR team

Y. Create schedule for AI workshops

Y. Reframe AI Workshop for new schedule / approach

Y. Create Issues for User Needs & AI Fit workshop + finalize mural


### Week of 2023-05-29
Y. Met with Diane O'Neal about KubeCon NA data collection partnership -> good to proceed 

1. CI "Early Adopters" draft report

Y. Get feedback from Valerie on draft of [Actionable Insight: Exploration Needed - SUS Verbatim Analysis + Workflow Links Discussion](https://docs.google.com/document/d/17-bgA4lzUl8WGLm6vyBFhR9nVnmkiKtWRrxcnp6IUf0/edit?usp=sharing) - see what steps she wants to lead 

Y. Wait to hear back from Jackie's stakeholders regarding whether Verify can participate in AI workshops  -> we are good to proceed

Y. ThinkBig on how to address need to refresh 

Y. Identified cohorts for Native Secrets Solution EArly Adopters program

Y. Identified dates for AI workshops

Y. participated in kick-off for Environment Design Sprint --> [created a video summary of Verify findings related to Environments](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/21#note_1413052311) Technically this is Will's team but I wanted to make sure that we incorporate findings from across research studies that I've run to date at GitLab 

### Week of 2023-05-22
Y. Get feedback from Jackie on draft of [Actionable Insight: Exploration Needed - SUS Verbatim Analysis + Workflow Links Discussion](https://docs.google.com/document/d/17-bgA4lzUl8WGLm6vyBFhR9nVnmkiKtWRrxcnp6IUf0/edit?usp=sharing) - see what steps she wants to lead 

Y. meet with team to craft approach to Early Adopters for Native Secrets solution

Y. start recruit for Early Adopters for Native Secrets solution

### Week of 2023-05-15
Y. MR for Persona table 

Y. MR for Handbook page on analyzing verbatim

1. Persona Table OKR and follow-up with MR

Y. Draft [Actionable Insight: Exploration Needed - SUS Verbatim Analysis + Workflow Links Discussion](https://docs.google.com/document/d/17-bgA4lzUl8WGLm6vyBFhR9nVnmkiKtWRrxcnp6IUf0/edit?usp=sharing) and get feedback from UXR team

### Week of 2023-05-08
Y. [MR for Handbook page on analyzing verbatim](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/124442?diff_id=678976759#diff-content-279cf6812ed93ea47749b4f1858b680c7cfeefb5) 

Y. schedule coffee chat with Katie (Tech Writing Manager) about onboarding template idea

Y. finalize Persona Table OKR and follow-up with MR

Y. Finalized [Q2 research prioritization issue ](https://gitlab.com/gitlab-org/ux-research/-/issues/2427)

Y. find partners for key terms table for CI/CD Catalog 

Y. Create [issue for GitLab Native Secrets Solution Early Adopter program](https://gitlab.com/gitlab-org/ux-research/-/issues/2470 ) and intro survey so that team can begin recruiting

Y. [ThinkBig for "Allow users to run specific sages or jobs associated with different pipelines"](https://gitlab.com/gitlab-org/gitlab/-/issues/407788). --> Based on the ThinkBig, the team has 3 next steps: (1) Finish local debugging blueprint, (2) Consider POC for cheap pipelines, (3) Consider Opportunity Canvas for Pipelines in VS Code 


### Week of 2023-05-01 (Erika OOO  - 05/01)
Y. CI Alpha Assignment 3 interviews

Y. Meet with stakeholders regarding research needs for Q2

Y. Score CI Alpha Assignment 3 and write-up summary + ~~draft deck for report~~ (we should have the study wrapped up by mid-June so I'll save it for one report that goes over all assignments)

Y. Persona Table OKR - presented in Product meeting

Y. (but we won't publish) ChatGPT vs. UXR comparison

Y. Draft of [Verbatim Analysis Handbook page](https://docs.google.com/document/d/1lH_H09Dcik55k-LTQeMw1U7m0BqS9OKLx5yAvqSKsv0/edit?usp=sharing) 

### Week of 2023-04-24 (Erika OOO  - 4/28)
Y. CI Alpha Assignment 3 interviews

Y. Work on Persona Mapping OK

Y. Work with Adam to draft OKR related to SUS Verbatim Cross-functional Analysis deck? + work on next steps

Y. Release CI Workflow Automations report

### Week of 2023-04-17 -- Erika OOO 
Y. Set up support for reminder emails for CI Alpha assigment 3 to go out

Y. Created discussion guide for CI Alpha assigment 3 interviews 

Y. Soft release of CI Workflow Automations report to team (double check on next steps)

Y. Release Benchmark Report and next steps 🙌

Y. Provided feedback on secrets related options for next steps -https://gitlab.com/gitlab-org/ci-cd/pipeline-security-group/-/issues/126 

### Week of 2023-04-10
Y. Finalize and send out CI Alpha Assignment 3 - We'll send out Assignment 3 on 2023-04-13 (or sooner in the week if inputs are ready). If Inputs aren't ready, we'll work them into Assignment 4. Particpants are primed and ready to receive that Assignment next week. Projects created for 14 participants. Due date is 2023-04-24. Submissions will go straight to the team in the meantime. 

Y. Meet with Darren to talk about research needs for next quarter

Y. Respond to feedback on SUS Verbatim Cross-functional Analysis deck

Y. Drafted [report on CI Workflow Automations](https://docs.google.com/presentation/d/1jb9yI6BFnmZxEYdEHGNs7QXGAgVSGVGvAlEHvNeeNXY/edit?usp=sharing)

Y. Provided feedback on the Private CI/CD Catalog design mocks


### Week of 2023-04-03
Y. Reach Goal: Write up SUS Verbatim Cross-functional Analysis

Y. Find someone to pilot the CI Alpha Assignment 3

Y. Work with Eng on CI Alpha Assignment 3 

Y. Ask stakeholders to engage with Q2 Research Prioritization Issue

Y. Help Sunjung with solution validation for Nav team - team is asking for approval based on the initial study and, if they don't get it, will use the pipeline editor entry point for the MVC

Y. SUS analysis: https://gitlab.com/gitlab-org/ux-research/-/issues/2428  

Y. Ask for feedback on SUS paper cuts -> https://gitlab.com/gitlab-org/ux-research/-/issues/2429

Y. Recruited 3 new users into our CI Alpha 

Y. provided questions from CI Alpha users, a concern and uses cases that we can address in Docs  - https://gitlab.com/gitlab-org/gitlab/-/issues/399225#note_1344348240 

### Week of 2023-03-27
Y. Actionable Insights for Benchmark Findings Epic 

Y. draft Q2 Research Prioritization Issue

Y. Compile list of SUS yml feedback users to invite to 2nd cohort in CI alpha program and send to Will

Y. verify and Package Stage Field sync

Y. CI/CD UX Meeting

Y. [Created a deck to summarize a key problem that we've observed in Verify research around Environments and Variables](https://docs.google.com/presentation/d/1WhIj2M1Rwk2LTHGZc4JU6ZvGamvDzWw2pXwAjnAVzoc/edit?usp=sharing) and shared this with the new Environments and Pipeline Security team 

Y. synthesized findings from the UXR Secrets research program for the switchboard team [here](https://gitlab.com/gitlab-org/ux-research/-/issues/2383#note_1333415921)

### Week of 2023-03-20
Y. Create an Epic for bechmark findings - [Benchmark Findings Epic &10124](https://gitlab.com/groups/gitlab-org/-/epics/10124)

Y. Create video for benchmark findings overview + SUS companion analysis

Y. Create video for workflow overview

Y. Create videos for author a pipeline tasks

Y. Create video for understad and adjust unit test tasks

Y. Create videos for find and fix pipeline error tasks

Y. Create videos for refresh theme

Y. Create videos for run entire pipeline each time theme

Y. Create videos for yml confusion theme


### Week of 2023-03-13 - OOO to return on 2023-03-17
Y. Respond to feedback from UXR team

### Week of 2023-03-06
Y. Finished Benchmark Analysis + Reach Goal of SUS Analysis

Y. [Draft of Benchmark Report](https://docs.google.com/presentation/d/13MQeN7Q-kIebc9lpIUHsn4_PKEv1XklWdD2tuvZkk8M/edit?usp=sharing)

### Week of 2023-02-27
Y. Benchmark Task Analysis (Try for 3-5 tasks) + start to build out slides

Y. Start to think about SUS Verify workflow analysis related to benchmark

Y. run lifecycle of an image sessions (up to 2) and process gratuity

N. create lifecyle of an image Dovetail project 


### Week of 2023-02-20
Y. Replace times for benchmark that were missed during coding in sessions (spreadsheet error)

Y. tag benchmark videos

Y. benchmark gratuity

Y. upload benchmark videos and tag tasks

Y. add to coding manual

### Week of 2023-02-13
Y. Benchmark Sessions

Y. CI Alpha Assignment Reminder and Gratuity

Y. Benchmark Recruit

Y. Tag Tasks in Benchmark videos

Y. Add to coding manual

### Week of 2023-02-06
Y. Create Doc with cut and paste formatted instructions for benchmark

Y. Table summarizing the Verify test environments

Y.[ Decision Rule - Benchmark Coding Doc](https://docs.google.com/document/d/1ne5u2HFU-Neea_eWuGv8OQcspzB2JftrcMvJiEcAXMY/edit#)

Y. create test accounts per benchmark participant

y. Benchmark Recruitment

Y. Ran 9 Verify Benchmark Participants


### Week of 2023-01-30
Y. Not very susccessful -  Recruit for benchmark study at SecOpsCon 

Y.  Collect responses for workflow automation game at SecOpsCon

y.  Summarize test environment requirements for benchmark and sync with Eng team

Y. Set up benchmark sessions slot 

Y. send out benchmark recruit

Y. Provide details on the CI Alpha program assignment distribution + support for Assignment 1

Y. Create spreadsheet for Verify Benchmark 

### Week of 2023-01-23
 Y. Ran 11 30 min pilots for benchmark tasks  - Schedule 2nd round of benchmark pilots

 Y.  Detailed look at Pipeline Execution tasks + Feedback

 Y.  Reach: Pilot Pipeline Authoring tasks

 N.  Detailed look at Package tasks (before Tim leaves) + Feedback

 Y. Schedule Task Sync sessions for 4/12 - 4/17
 
 Y. Schedule Stage Workshop for week of 4/25 - 4/28

### Week of 2023-01-16 (OOO on 01/16 for Holiday)
Y. Run pilot sessions for Pipeline Insights Benchmark tasks  - https://gitlab.com/gitlab-org/ci-cd/pipeline-insights-group/-/issues/136 

Y. Finalize CI components sample + set up emails for that group 

Y. Summarize baseline feedback on current CI templates experience

Y. ~~NOT WORTH IT: Look into package JTBD and tasks to see if it's possible to combine data collection~~

Y. Set up Test environment issue thread

Y. work with Manuel and Dov to finalize Pipeline authoring tasks

Y. Set up Promo game / gratuity for SecDevOps conference + CI automation game + setup QR codes

### Week of 2023-01-09

Y. Coordinate a common screener for Sol.Val for CI/CD - https://gitlab.com/gitlab-org/ux-research/-/issues/2284

Y. Draft changes to the handbook on Common Screener process steps

N. Get feedback on environment requirements - completed but we haven't cleared this yet because we haven't finalized our tasks

Y. Create assignments for CI components Alpha Program - https://gitlab.com/gitlab-org/ux-research/-/issues/2289

Y. Recruited n = 4 participants for the external CI Components Alpha Program - compiling participant info here

Y. Set up surveys for all 5 CI Components Alpha Program "Assignments", which are described and linked in the research issue description + shared with internal Dog Fooding team

Y. Drafted CI automation game - https://gitlab.com/gitlab-org/ux-research/-/issues/2034#note_1237911555 

### Week of 2023-01-02 (000 -> until 2022-01-04)
Y. Ask for feedback on Verify workflows and tasks 

Y. Help desginers to create video overviews of the tasks and work with them to refine from there

Y. Created Package Benchmark Epic -  &9587 - https://gitlab.com/groups/gitlab-org/-/epics/9587

Y. Create an outline for the CI conponent Alpha Program - https://gitlab.com/gitlab-org/ux-research/-/issues/2289

OOO from 2022-12-26 to 2023-01-03

### Week of 2022-12-19
Y. Got the Benchmark Loop Stage Common Screener Ready to go

Y. Found pilot participants for Plan Benchmark

Y. Drafted workflows and tasks for Verify with the team

Y. Set up epic and supporting issue for the Veriy Benchmark

Y. Proivded feedback on Artifacts page Sol validation survey - https://gitlab.com/gitlab-org/ux-research/-/issues/2202 

Y. Met with Sunjung (new designer for Pipeline Authoring)

Y. Pulled some contacts for https://gitlab.com/gitlab-org/ux-research/-/issues/2245#note_1216783891 + drafted an example recruitment email 

Y. Put in data request for Siense spreadsheet to help inform our test environment for benchmark - https://gitlab.com/gitlab-data/product-analytics/-/issues/937 

### Week of 2022-12-12
Y. Created project plan for Verify Benchmark - https://gitlab.com/groups/gitlab-org/-/epics/9494

Y. Drafted criteria for Verify benchmark participants

N. Verify and Package Research Registry and Synthesis 

N. Draft report for Verify and Package Research Registry and Synthesis 

Y. Publish the Leadership Integrations Survey Report

Y. Check in to support the  Benchmark Study Common Screener

### Week of 2022-12-05 on PTO

### Week of 2022-11-28
Y. Follow-up with James on ThinkBig -> James will articulate an overall vision for the teams to pickup with in the ThinkSmall

Y. Start thinking through the Verify and Package Research Registry and Synthesis (added 2 studies to the synthesis and started mapping out the report format)

Y/N. Finalize report for Leadership Integrations Survey Analysis - the report is final on my side, but Grant wants to sit with next steps for longer

Y. Finalize report for benchmark sample cross-stage alignment  - [[REPORT] Benchmark Cross-Stage Alignment: Screeners, Samples + Scores](https://docs.google.com/presentation/d/1MqxPlxzY2Id6D-Q_oZjsEHFEwMz8zZO8sUPhY7tG88s/edit?usp=sharing)

N. Give Tim an overview of Benchmarks Process - He didn't have the time, but we were able to get a list of core tasks to include in the benchmark screener

Y. Set up the Benchmark Study Common Screener which can now cover -  https://gitlab.com/gitlab-org/ux-research/-/issues/2246

### Week of 2022-11-21 (OOO 11/24 + 11/25)
Y. Leadership Integrations Survey Analysis + data pull for the CI automation study
Y. Draft report for Leadership Integrations Survey Analysis
Y. Draft report for benchmark sample cross-stage alignment

### Week of 2022-11-14
Y.   Update on Leadership Integrations Survey and process gratuity

Y.   Support for ThinkBig on Secrets + Follow-up

Y.   Comparison of Benchmark Recruitment Screeners https://gitlab.com/gitlab-org/ux-research/-/issues/2182#note_1171743848 + drafted a [report on the differences and what they mean in terms of our ability to compare performance across studies](https://docs.google.com/presentation/d/1MqxPlxzY2Id6D-Q_oZjsEHFEwMz8zZO8sUPhY7tG88s/edit?usp=sharing)

Y.   start on Leadership Integrations Survey Analysis 

N. (Reach Goal) Start thinking through the Verify and Package Research Registry and Synthesis Update 


### Week of 2022-11-07 (OOO Veterna's Day)
Y. AMA for Secrets Features Prioiritization Survey 

Y. Finalize Q4 research planning issue + supporting issues 

Y. Secrets policy report - https://docs.google.com/presentation/d/1P2mDpw9nazrF1GfilPAl3V-D3f5tPcj_ubv5GRxzQFg/edit?usp=sharing

Y. work on secret scan solution validation - Draft: https://docs.google.com/presentation/d/1aO48NXHdA_A9BvmpYw5ZlA1Le78rbqH8uOa7X3N7X58/edit?usp=sharing 

Y. Promo-game sent out for secrets features 

N. Update on Buyer Survey 

### Week of 2022-10-31
Y. finalize Self-eval

Y. [Finalized report for Secrets Features Survey](https://docs.google.com/presentation/d/1GjpnLVr3KwpxAU_TuLZgi9BoNoCPxT0pNR1JYd4AYis/edit?usp=sharing)

Y. Q4 research planning issue + supporting issues

Y. Meet with Tim async about Package benchmark. He wantes to do it bue won't have eng support until 15.8 (1/22) or 15.9 (2/22)

Y. Report on metadata & secrets -> https://docs.google.com/presentation/d/1jqxW5MCm2jBVkfIg0TOARI3cTnsG6qBqxJN5u4e-lx0/edit#slide=id.g12b319f6181_0_0 

Y. support Veethika in collecting data at All Things Open -> https://gitlab.com/gitlab-org/ux-research/-/issues/2200 


### Week of 2022-10-24
Y. Draft Self-eval

1. Respond to Kevin's design ideas around secrets

Y. Move all secrets related KubeCon Games into Qualtrics format

Y. Create a how-to video for Dov, James and Gina

Y. Ask team to share social media post to promote secrts survey

N. Attend Verify and Package field sync update and post UXR update

Y. Take notes and provide support during Buyer Persona focus group 

Y. Set up automatic scoring for Buyer Persona focus group questions

Y. Serve as support in the UX pit for KubEcon NA

Reach: Created mini-brief report for KubeCon NA focus group 


### Week of 2022-10-17
Y. Draft the policy in the workflow study + Reframe the workflow game -> created decks for each set of booth interviews

Y. Set up offline access for KubeCon NA Data Collectors who have Qualtrics accounts

Y. Set up Qualtrics account for those who don't have one to use as a common account

Y. Run KubeCon NA Research Part of Know Before You Go Session on 10/19

Y. Met with Kevin to talk about designs

Y. Update the team on the Metadata game / pilot


### Week of 2022-10-10 (OOO 10/10 + 10/11)
Y. Run Survey Development Interviews for https://gitlab.com/gitlab-org/ux-research/-/issues/1819

Y. Work on Automation study + Feedback Issue -> refine research questions -> as first step we're including this in the KubeCon NA on floor interviews

Y. Build out framework for the KubeCon NA interviews in Qualtrics

Y. Created a Know Before You Go Deck to train booth staff on KubeCon NA research efforts -> https://docs.google.com/presentation/d/1pc8b3FXaKeHO4BzMBnF3iQ477aW8jzozvUyv0ydAoR4/edit?usp=sharing

Y. Create the promotional game for Enterprise Integrations Study + get new target sample size based on population estimates

Y. Provide an update on numbers so far and any trends that we're seeing in Eterprise Respondents for the Secrets Features Prioritization Survey - save a report with the data as of 10/19

Y. Take notes at CAB 

Y. Set up Pre-Game Survey 


### Week of 2022-10-03
Y. Ran 4 Survey Development Interviews for https://gitlab.com/gitlab-org/ux-research/-/issues/1819 

Y. Refactor KubeCon NA Studies, create new issues for those and assign one DRI per PM/UXR to lead each one -> carved out one solid study and created the interview template and piloted it -> https://gitlab.com/gitlab-org/ux-research/-/issues/2138 

Y. Built out study for MetaData we need to have around secrets to develop a built-in soluiton -> https://gitlab.com/gitlab-org/ux-research/-/issues/2138 

N. Work on Automation study + Feedback Issue

Y. Attend a couple of Verify Team Building Sessions in Seattle 

Y. Create a backup plan for the Enterprise Integrations Study reruitment + adjust schedule 

Y. Create a 2nd survey issue for Enterprise Integrations Study and ask for it to be included in the next All Customer Survey  -> I've set up a related proposal where we'd include 6 questions with our list of product integrations in different areas out with our next customer wide survey, in Jan 2023. #2134
 
### Week of 2022-09-26
Y. Finalize and share reports from Secrets workflow study - https://gitlab.com/gitlab-org/ux-research/-/issues/1974#note_1119932339  

Y. Follow-up with Kai Armstrong - PM for Code Review on report about the blocked MR workflow (potential issues + follow-up questions)

Y. Follow-up with PM Connor Gilbert and PD is Michael Fangman about Block MR, Secrets Policy and Congif workkfow (potential issues + follow-up questions)

Y. Vote in system performance workshop

Y. Create issue with Draft of RBAC study / desk research project and get feedback from cross-functional team 

Y. Help with recruitment efforts for https://gitlab.com/gitlab-org/ux-research/-/issues/1819 

Y. Create an issue with background research to support onboarding for new Desginer  -> https://gitlab.com/gitlab-org/ux-research/-/issues/2119 

### Week of 2022-09-19
On hold. Create Proposal for 1/1 Gratuity for Secrets Feature Prioritization Survey~~ Caitlin doesn't think this will help and we're halfway to our requirement. 

N. (waiting for Dov to outline next stepts + creating a meta-summary report_ Finalize and share reports from Secrets workflow study 

Y. Follow-up with Dov re Next steps for Secrets in Workflow Reports

Y. Started conversations with stakeholders on next steps for research / product

Y. Work on summary and video tagging strategy for Package TouchPoint Interviews  - did this with one video, but stopped there becuase I want to review those in prep for the Lifecycle of an image study, which Tim wants to wait to run

Y. [Drafted an RBAC study in word doc](https://docs.google.com/document/d/1QKZS2T1HJw6ZZhAWV8I674Ql1D-jfD_9RMDXexO0s2o/edit?usp=sharing) and reviewed with Dov 
Y. Met with Tim and Katie to recalibrate research projects 

Y. Helped to reframe and focus the Enterprise Integrations study - created planning doc - created practice report - refined and iterated. https://gitlab.com/gitlab-org/ux-research/-/issues/1819 

### Week of 2022-09-12
Y. Write up report brief for using raw screts values in the debugging workflow - https://docs.google.com/presentation/d/1OBIE08F95CGJ8DTyUiS-yIHw9uhsYaWRuT6zmHJmNSo/edit?usp=sharing 

Y. Analysis for using raw secrets values within a config file - https://docs.google.com/presentation/d/1pbNCEkRT7uVZO4T7dISo0qnXqZspVA1YSwsjGRrwX_8/edit?usp=sharing 

Y. Analysis for using workflow when MRs are blocked by secrets - https://docs.google.com/presentation/d/1du7-Yss4_b5CMXFU_QXmhrH1vKIXwIreT-x41uQ6IZ4/edit?usp=sharing 

Y. Analysis for creating secrets policy workflow - https://docs.google.com/presentation/d/1sTC-Z9dyofVCb85x3W1r8tuw8M2feer3tH76BSprYOc/edit?usp=sharing 

Y. Track number of survey responses from email campaign

Y. provided feedback on #1663 for Katie - excited to see where she takes the study - https://gitlab.com/gitlab-org/ux-research/-/issues/1663#note_1099571593 

N. (was sick) attend system performance workfshop or watch recording


### Week of 2022-09-06
Y. run final feedback sessions

Y . finalize survey

Y. finalize promototional games with legal -> https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/6910#note_1093685204 

Y. publish survey

Y. begin summarizing learnings from secrets workflow sessions

Y. work to coordinate research efforts across all KubeCon projects - https://gitlab.com/gitlab-org/ux-research/-/issues/2108

### Week of 2022-08-29 (PTO (friends & family day): 2022-09-02)
Y. Develop secrets workflow Pre-Game + Gameplay Boards

Y. Run pilot sessions for workflow game + get feedback on survey (8 sessions total)

Y. updates to the secrets features survey

N. Reach Goal: dratf a discussion guide for Internal study of how teams are using internal DevOps Resources [#2075](https://gitlab.com/gitlab-org/ux-research/-/issues/2075) so that Nadia + Dov can use during interviews with the security teams


### Week of 2022-08-22
Y. set up promotional game #1 for gift cards for email campaign + recruitment support + gratuity issues for Secrets features survey (1st run Sept 7 - Oct 19)

Y. set up promotional game for Sec/CI/CD training certicates at KubeCon NA

Y. work to finalize secrets features survey  - incorporate feedback

Y. work to set up additional sesions for secrets features survey

Y . draft approach and tasks for the sectes workflow exercise

Y. [draft approach to Commit data collection](https://docs.google.com/document/d/11hNAcJikaB7_I7lVCSF8Fv6DIGc1n7SaJpNPy_OPXbs/edit?usp=sharing) --- create an issue[ #2082](https://gitlab.com/gitlab-org/ux-research/-/issues/2082) so it's easier to track this work as a sub-step to the participatory design excercise. 

Y. Met with Katie to talk about her projects + provided feedback on the [technical investigtion that is a prestep to the Migrate to Package research plan](https://gitlab.com/gitlab-org/gitlab/-/issues/298726#note_1078607702)

### Week of 2022-08-15
Y. Survey development sessions - > https://gitlab.com/gitlab-org/ux-research/-/issues/2044  (n = 13 Total)

Y. Create mock-up of [Secrets Features Prioiritization Survey Report based on pilot participants](https://docs.google.com/presentation/d/1jcpC-MxIdqvPsZOca6CUN1DfoxKwROx60wFOJNfoync/edit?usp=sharing)

Y. Refactor research prioiritzation to include new studies + adjust survey features project

Y. Create [draft of Internal study of teams and how they're using internal DevOps Resource Library :second_place: issue ](https://gitlab.com/gitlab-org/ux-research/-/issues/2075)

Y. [Assist Nadia with her Secrets Management JTBD meta-analysis](https://gitlab.com/gitlab-org/ux-research/-/issues/1981#note_1056765002) - we used the MadLib activity and seh reviewed past research - https://gitlab.com/gitlab-org/gitlab/-/issues/359047/ 

Y. Finalize draft of an issue where we'd like to user facing teams (e.g., TAMs)  to "log" notes and observations around what users needed to learn about or understand in order to use DevOps resources --> https://gitlab.com/gitlab-org/ux-research/-/issues/2053.

Y. Helped to draft the framimg for Veethika's study so it's clear how it's distinct from UXR Benchmarking - https://gitlab.com/groups/gitlab-org/-/epics/8579#note_1064990376, https://gitlab.com/gitlab-org/gitlab/-/issues/361449, https://gitlab.com/groups/gitlab-org/-/epics/8579

Y. worked to communicate what Jackie had in mind for Katie's ucpoming studies - https://gitlab.slack.com/archives/CL9STLJ06/p1660781679907979
Y. drafted a summary of oneof Jocelyn + Gina's interview as quick excecise to help us set up the Company Profiles + Personas work  -> https://gitlab.slack.com/archives/CL9STLJ06/p1660855031935379 

Y. provided quick feedback on Veethika's study - https://gitlab.com/gitlab-org/ux-research/-/issues/2036



### Week of 2022-08-08
Y. Ran 11 sessions -  [Schedule + run users sessions for secret features survey development ](https://gitlab.com/gitlab-org/ux-research/-/issues/2044)

Y. Post key learnings and questions from sessions for team and work to refine secrets features survey

Y. [Content scrub: UXR and UXR Ops Coordination handbook pages](https://gitlab.com/gitlab-org/ux-research/-/issues/1968) 

Y. Presented on KubeCon data collection success as a guest speaker at product meeting

N. (did start scoping out study in Q4 w Jackie) Reach Goal - [Support Katie async to create a foundational research project around secrets and Package area](https://gitlab.slack.com/archives/CL9STLJ06/p1659704658784259) 


### Week of 2022-08-01
N. [set a schedule so that we do that with practice data + made that a reach goal] Create draft of report for secrets feature prioritization survey (work backwards to clarify goals)

Y. [Work with team to finalize draft of features and definitions for feature prioritization survey](https://gitlab.com/gitlab-org/ux-research/-/issues/2002)

Y. Meet with Marketing to go over KubeCon NA plans - [Potential blocker for participatory design activity -> started working on back-up plans ](https://gitlab.com/gitlab-org/ux-research/-/issues/1974#note_1052846497) / 

Y. Create issue to accrue feedback on GitLab DovOps Library + learnability, with a focus on CI/CD -> #2053

Y . Schedule user sessions for secrets feature prioritization survey + pilot sessions of participatory design activity

Y. Bonus: [Ran 2 users sessions for secret features survey development ](https://gitlab.com/gitlab-org/ux-research/-/issues/2044)

Y. Bonus: Got approval to give away Security Certifications at KubeCon NA 

Y. Bonus: [Set up issue for KubeCon NA Blog](https://gitlab.com/gitlab-org/ux-research/-/issues/2062)

N. [On hold - waiting for UXR leadership to support - I think we need a 3rd category for these ] apply labels to Actionable insights for [Secrets Lifecycle - user secret typology report ](https://docs.google.com/presentation/d/127AIPu5cCPDDdScuxN-nbQPKhgKl9I77jYd-x3_tZEM/edit#slide=id.g13a59943c83_0_0) 

N. [Content scrub: UXR and UXR Ops Coordination handbook pages](https://gitlab.com/gitlab-org/ux-research/-/issues/1968) - completed 1/3

### Week of 2022-07-25
Y. Finalize Q3/Q4 research plans

Yes, but still need to label them. Actionable insights for [Secrets Lifecycle - user secret typology report ](https://docs.google.com/presentation/d/127AIPu5cCPDDdScuxN-nbQPKhgKl9I77jYd-x3_tZEM/edit#slide=id.g13a59943c83_0_0)

Y. Think about KubeCon NA research Projects

Reviewed, but waiting to make changes until I meet with Adam. [Content scrub: UXR and UXR Ops Coordination handbook pages](https://gitlab.com/gitlab-org/ux-research/-/issues/1968)

### Week of 2022-7-17 ---> OOO {return on 7/25}

### Week of 2022-7-11
Y. Finalize report for for Secrets & Infrastructure Lifecycle Study [#1873](https://gitlab.com/gitlab-org/ux-research/-/issues/1873)

1. Reach Goal - create actionable insights + async feedback issue for [#1873](https://gitlab.com/gitlab-org/ux-research/-/issues/1873)

N. UXR Handbook pages 

N. Reach Goal - start Q3 research prioritization issue 

Y. Reach Goal - look into draft of secrets features Kano survey + prep for wide feedback 

### Week of 2022-7-05
Y. Draft report + Work on analysis for Secrets & Infrastructure Lifecycle Study [#1873](https://gitlab.com/gitlab-org/ux-research/-/issues/1873)

N. Sawtooth survey tool training 

Y. support for business case for built-in secrets

Y. Provided feedback on  Buyer Persona Survey Instrument [#6388](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/6388)

N. (reach goal) UXR Handbook pages

### Week of 2022-06-27
Y. Create [issue for secrets/security feature prioritization survey](https://gitlab.com/gitlab-org/ux-research/-/issues/1970) + updated the survey issue with new timeline

Y. Created an [issue to support providing 3 security training certifications as a prize for our DevOps survey ](https://gitlab.com/gitlab-com/customer-success/professional-services-group/education-services/-/issues/453 for KubeCon NA )

Y. Attend Verify field Sync 6/29

Y. Work on analysis for Secrets & Infrastructure Lifecycle Study [#1873](https://gitlab.com/gitlab-org/ux-research/-/issues/1873)

Y. Looking into GitLab Undergraduate Education Pipeline  - https://gitlab.slack.com/archives/CMELFQS4B/p1656351379372929 

Y. Analysis for team's JTBD study + analysis template for future research - https://gitlab.com/gitlab-org/ux-research/-/issues/1956#note_1012703759

Y. Provided [feedback on Marketing research project -> Buyer Personas](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/6388#note_1012344719)

### Week of 2022-06-20 (3 day week)
Y. Update Research prioritization issue with new goals

Y. Get feedback + linking/creating actionable insights for SaaS Enterprise User Base Survey in [async discussion issue](https://gitlab.com/gitlab-org/ux-research/-/issues/1988) 

Y. UX roadmapping workshop | Pipeline Insights

Y. Work on analysis for Secrets & Infrastructure Lifecycle Study [#1873](https://gitlab.com/gitlab-org/ux-research/-/issues/1873)

### Week of 2022-06-13
Y. Draft report for SaaS Enterprise User Base Survey 

Y. Recalibrate research prioritization issue + meet with Jackie

N. Create survey for core team members to ask what type of notification they'd like to recieve

Y. Finalize next steps for SaaS Shared Runner Survey & Share out

Y. {Bonus} Drafted discussion guide for https://gitlab.com/gitlab-org/ux-research/-/issues/1798 

N. {Reach Goal} Work on teams analysis for Veethika's study

### Week of 2022-06-06 {Tested Positive for Covid, mainly working async}
Y. Record KubeCon EU Ops Product Direction Survey Share-out

Y. Finalize Report for [Shared Resource library + CI Templates](https://docs.google.com/presentation/d/1Xu2tgqBC4n-nkrr4jSRcydpbEMKh63LskaAaqWPum4w/edit?usp=sharing) + [Executive Summary Report ](https://docs.google.com/presentation/d/18G8lUOHpCk2cIY2HCdROn3-IWrBgZkG9etpFM9bOxrA/edit?usp=sharing)

Y. Provide marketing with sample for Case studies and interviews recruited as part of UX survey during KubeCon EU

Y. Met with Veethika and we closed out her study  - https://gitlab.com/gitlab-org/ux-research/-/issues/1909 

1. Met with Erica in Marketing about KubeCon North America data collection 

1. described the relationship between the 5 observed team types in our Shared resource library study + Team Toplogies work 

1. started analysis for SaaS Enterprise User Base Survey 

1. Created research plans for Secrets Features Kano Survey + Participatory Design activity

### Week of 2022-05-30
Y. Meet with Jackie to talk about priorities

Y. Finalize draft and share out KubeCon EU 22 Report with core PM team

N. [Blocked] Generate Actionable Insights, and share out Report on Shared Resource Library + CI Templates  - need team input on next steps

N. [Focused instead on closing out KubeCon Ops Product Direction Survey next steps with Darren] Finalize next steps for SaaS Shared Runner Survey

Y. Created promotional game for Runners JTBD study + adjusted surveys 

Y. Provide feedback on script for Pipeline Components MVC #1942

Y. Provide summary of findings for KubeCon EU survey

Y. UXR team support: Draft deck template for marketing team + draft a UXR report slack channel rules

N. {Reach Goal} Work on teams analysis for Veethika's study [Veethika still out]

### Week of 2022-05-23
Y. Meet with Nadia to go over findings fom 1873 + generate next steps

Y. Meet with Darren to sync on report for SaaS Shared Runner survey 

Y. Found prospective participants for [1942](https://gitlab.com/gitlab-org/ux-research/-/issues/1942) + [1851](https://gitlab.com/gitlab-org/ux-research/-/issues/1851) 

Y. Assis with preparing JTBD survey for promotional game for https://gitlab.com/gitlab-com/Product/-/issues/3954 

1. Finalize analysis once survey data collection ends on 5/25

Y. [KubeCon EU 2022 UXR Field Interview Notes / Summary](https://docs.google.com/document/d/1Xds0uG7nK-Esap_6N-bkiU4m_stmAC4u1rNxVhvuhAE/edit?usp=sharing)

Y. Run survey Sweepstakes

Y. Create slides for Marketing Deck Shareout on KubeCon

Y. Attended Pipeline Authoring &  Pipeline Execution meeting

### Week of 2022-05-16
Y. KubeCon EU Data Collection - successfully fielded 15 companion surveys 

Y. {Reach Goal} SaaS + Self Managed CI build environments 🥈 -> Analysis for Darren

Y. {above and beyond} SaaS + Self Managed CI build environments 🥈 -> Created a report for the team + completed follow-up analyses based on feedback

Y. {Reach Goal}  :airplane_arriving: analysis and draft of report for Issue #1873  + created an issue to coordinate feedback from the team

Y. {Reach Goal} :airplane_departure:  analysis and draft of report for Issue #1740  - [report is fully drafted](https://docs.google.com/presentation/d/1R4N4ZB2F67L0tjXkl-gUNsfFbBTmorqa7EbgMbFbJ5Y/edit?usp=sharing), waiting to finalize until 5/25 when we'll close out the survey



### Week of 2022-05-09
Y. Attended sessions for https://gitlab.com/gitlab-org/ux-research/-/issues/1909 and provided feedback

Y. Pulled prospective participants for 1909 + 253

Y. Discussed Analysis plan for https://gitlab.com/gitlab-org/ux-research/-/issues/1909

Y. Set up analysis partner schedule time-blocks for KubeCon EU survey results review

N. Finalize Draft of Report for Infra Lifecycles and Shared Resource Library - expanding scope to include CI Template (team has reprioritized this)

Y. KubeCon travel preparation

Y. Publish webpage for KubeCon survey promotional game 

Y. Create coordination issue for support tasks from Dov + Nadia 

### Week of 2022-05-02
Y. Meet with teams to finalize Q2 Research Prioritization Issue

Y. Sprint on lifecycle analysis for infrastructure report

Y.  Create an analysis plan for the Lifecyles Study

Y. Create an execution plan for Design Principles / Hueristic Analysis proposal

Y. Run a few more LifeCycle session with participants from companies with 10,000+ employees

Y. Set up analysis plan with team for Ops User Base survey

### Week of 2022-04-25
OOO 4/25

Y. Ran 14 LifeCycle sessions + Created a summary of participant details

Y. Provided feeback on Gina's design mocks + Nadia's Solution Validation pilot study

Y. Updated Q2 Research Prioritization Issue

Y. Iterated on plan for Design Principles / Hueristic Analysis proposal

Y. Created an analysis plan for the Ops Product Direction Survey -> broke our secrets report and workflow report

Y. Performed a practie analysis with n = 2 early survey respondents for the Ops Product Direction Survey 

### Week of 2022-04-18
OOO 4/22
Y. Run pilot sessions for Lifecycles of Secrets and Infra Resources study - 

Y. Scope our plan for Lifecycles of Secrets and Infra Resources study  

Y. Create Design Principles / Hueristic Analysis proposal

Y. Draft Q2 Research Prioritization Issue

Y. Decide next steps for SaaS Shared Runner Survey and/or help Darren with analysis

Y. Create landing page for KubeCon Survey promotional game

### Week of 2022-04-11
Y. Coordinate a Spring/Summer 2022 Common Screener

Y. Draft Script for Secrets and Infra Resources study

Y. Ran 4 pilot sessions for Secrets and Infra Resources study

Y. Coordinate work plan around KubeCon events (will work on weekend during travel)


### Week of 2022-04-04
Y. Created proposals for Promotional Games for the User Base, KubeCon Ops Product Direction, and Darren's SaaS Shared Runners Survey

Y. Start coordinating Q2 research prioritization issue + tagged team

N. Ops Prod Direction Survey ->  Qual themes analysis

Y. Drafted Spring/Summer 2022 Common Screener

Y. Found participants for Rayanna's study

Y. Worked with team to draft research questions for [1873](https://gitlab.com/gitlab-org/ux-research/-/issues/1873)

Y. Provided update / feedback on Research Prioritization issue

### Week of 2022-03-28

Y. Ops Prod Direction Survey ->  Survey Adjustments report to team

Y. Drafted User Base survey to help us understand preferences related to secrets and SaaS adoption barriers within our user base -> https://gitlab.com/gitlab-org/ux-research/-/issues/1867

Y. Help CICD Catalog team with study recruit

Y. Meet with Jackie and Tim/Katie to talk about research priorities 

Y. Created an issue for a Package research program that will bring in users for general feedback on a regular cadance - https://gitlab.com/gitlab-org/ux-research/-/issues/1870#note_898145953

Y. Provided feedback and drafted a Qualtrics survey for Problem Validation: Container Registry Info & File Naming study -> https://gitlab.com/gitlab-org/ux-research/-/issues/1868 

N. [Reach] Coordinate a Spring 2022 Common Screener

N. Ops Prod Direction Survey ->  Qual themes analysis

### Week of 2022-03-21
 Y  Run survey development interviews for [#1842](https://gitlab.com/gitlab-org/ux-research/-/issues/1842) 

 Y Ops Prod Direction Survey development interviews + survey instrument development analysis + updates to team

 Y  working with UXR team to determine how to support [#1847](https://gitlab.com/gitlab-org/ux-research/-/issues/1847) becuase it falls under the Foundations and other UXR teams

 Y Created JTBD survey template based on pacakge approach for Gina and Jackie for Build Artifacts work [#3954](https://gitlab.com/gitlab-com/Product/-/issues/3954)

 Y Drafted a Naming Survey (for the CI/CD Catalog) [#355668](https://gitlab.com/gitlab-org/gitlab/-/issues/355678) so that the team could see what the approach would look like - got feedback on names from a user

 Y Met with Laura Clymer to talk about KubeCon Europe

 Y Posted in the [Verify Stage Think Big - FY 23 Q1](https://gitlab.com/gitlab-org/verify-stage/-/issues/190#note_887609641)

 Y Met with Ashley to talk about background and to start brainstorming possible approaches for Settings work
 

### Week of 2022-03-14
 - Erika OOO 3/14 - 3/16 

Y.  3/16 present [Ops Product Direction Survey Step 1 Report](https://docs.google.com/presentation/d/1KllE2ot3gLrhzSE3LaQaxl2pRrU8L-OtQM3SdZ9EDl0/edit#slide=id.g1175a1ddd1d_0_6) at UX Showcase

Y.  Created a new issue for SaaS Shared runners -> https://gitlab.com/gitlab-org/ux-research/-/issues/1842 + Created a Recruitment issue for survey responses

Y.  Met with Darren to coordinate on a Mixed Methods approach to [#1842](https://gitlab.com/gitlab-org/ux-research/-/issues/1842)

Y. Ran 3 survey development interviews for [#1842](https://gitlab.com/gitlab-org/ux-research/-/issues/1842) and key questions for Ops Prod Direction Survey

### Week of 2022-03-07
 - to make sure that teams didn't get blocked, I worked an exra day on 3/11 (instead of climbin) to create an action plan for  [#1847](https://gitlab.com/gitlab-org/ux-research/-/issues/1847)  - - -> https://docs.google.com/document/d/1CCIyAiUuRP9qKzFOtgo0I_xOZz42oYNglQvsHzNDa7c/edit?usp=sharing 
 - Sprint on [1753](https://gitlab.com/gitlab-org/ux-research/-/issues/1753) - rework research questions and study scope + draft survey + create a timeline + look at logistics for larger survey
 - Create video summary for Step 1 Ops Product Direction Survey findings + start to socilalize
 - Create a plan for drafting a more compliance focused survey response item with the Manage: Compliance team
 - Integrate Designer feedback on Product Directions in to workflow themes summary (might need some chats with them)
 - Provided input on GitLab Benchmark approach to help focus analysis on overall stage level score and workflow analysis


### Week of 2022-02-28
Y. [Survey Instrumentation Report](https://docs.google.com/document/d/19slOtpUH4LKqu5LGy6nBp0UALy1mfXa8bG927sWQCYI/edit?usp=sharing)

Y. [Updates: Ops Product Direction Survey KubeCon Europe 2022 - Survey Planning Doc](https://docs.google.com/document/d/1X-WujtKdhN_k57emfakbnzPGJZbOcTuWFOAJga2hTIA/edit?usp=sharing) + Ping team members to provide feedback in sections

Y. Finalize Draft of [[Working Draft]  Survey Interviews Qualitative Themes Table](https://docs.google.com/document/d/1Lg6X-SkIO9oLePXUtYjDNdSxqfakgJs01fUYDmJAVhA/edit#)

Y. Share out update on Ops Product Direction Survey and ask team to draft tool tips

Y. [Draft Step 1 Report Deck to summarize high level findings from the first round of Ops Product Direction Survey Development Interviews](https://docs.google.com/presentation/d/1KllE2ot3gLrhzSE3LaQaxl2pRrU8L-OtQM3SdZ9EDl0/edit?usp=sharing)

Y. [Ran and summarized CI Private Catalog sessions](summarized in terms of research questions) + Setup Dovetail + found a few more participants for the team

Bonus: Found some prospective participants for [#1724](https://gitlab.com/gitlab-org/ux-research/-/issues/1724) + helped to draft discussion guide to see how prevalent the issues we saw with [Company A](https://docs.google.com/document/d/1vPnvLScOsuIqHooddXoTHcbA-vFxgm87diq8jqjNFZ4/edit?usp=sharing) are 

### Week of 2022-02-22
Y. Have  [Company A Summary](https://docs.google.com/document/d/1vPnvLScOsuIqHooddXoTHcbA-vFxgm87diq8jqjNFZ4/edit?usp=sharing) vetted by leading TAM

N. [Still putting together a few more details related to Qual findings] Summarize learnings from and survey changes made during Ops Product Direction Survey Development Interviews ->https://gitlab.com/gitlab-org/ux-research/-/issues/1803 

N. Share out update on Ops Product Direction Survey and ask team to draft tool tips - waiting to share out until after I review with Jackie on Wed 3/2. I reviewed with Tim last week. 

Y. Make a call for support on Recruit for GitLab CI for non-SCM users + Enterprise Case Study participants at Verify Field Sync -> this has yielded one SMB prospect so far

Y. CI Private Catalog: https://gitlab.com/gitlab-org/ux-research/-/issues/1790  -- Met with team to create a plan for piloting study sessions + scheduled sessions


### Week of 2022-02-14
N. Blocked - waiting for Caitlin to respond to my draft of next steps so that we can share with team-> Write up process steps for teams to use the Common Screener approach + share  with team [Collaborate with Caitlin]
Y. Summarize Lululemon background + draft with team + Send out recruitment email -> [Company A Summary](https://docs.google.com/document/d/1vPnvLScOsuIqHooddXoTHcbA-vFxgm87diq8jqjNFZ4/edit?usp=sharing)
Y. [Close out 90 day plan](https://gitlab.slack.com/archives/CL9STLJ06/p1645549215563639) + added in a [Google feedback form](https://forms.gle/ccYUtbE5cWN4fJeC6) 
Y. Ops Product Direction Survey -> Run survey development sessions -> 7 Sessions Total
Y. Create an intro deck for the Verify field sync to ask for help with the SCM + GL CI study and Enterprise Case Study recruit -> [TAM/SA Recruitment Deck](https://docs.google.com/presentation/d/1JzMQmBNfvb5Tnr472QkT9qqJGxJ702bnP5Vo3PQLypw/edit#slide=id.g115e1494e77_0_456)
Y. [Bonus]  CI Private Catalog: https://gitlab.com/gitlab-org/ux-research/-/issues/1790 -> [Created a discussion guide planning doc](https://docs.google.com/document/d/17ieiI3rqykjsAI9E9hgduIYlhzT91nRpWDV8cB86aXA/edit?usp=sharing) + [interview deck] https://docs.google.com/presentation/d/1g8uvgr7Iq3LidK7LX1WNM03--an1wb5ohWdrAvzzlik/edit#slide=id.g105d4eaebf5_0_1837

### Week of 2022-02-07
Y. CI Private Catalog: https://gitlab.com/gitlab-org/ux-research/-/issues/1790 -> Provided feedback on study goals and research questions, we decided to focus on competitors, created a recruitment issue for the team, pulled a sample of 5 prospect participants from common screener pool, + created a screener for the team in doc and qualtrics to support a more targeted Enterprise recruit

Y. Commom Screener Retro + Handoff

Y. **BLOCKED:** Supporting Recruit for external SCM (Source Code Management) with GitLab CI (Continuous Integration) -> Recruitment call went out in Sales Newsletter, not successful, closed out survey issue with a bulleted list of findings + summary spreadsheet

Y. Conduct interviews to get more background on Company A case study (also supports SCM -> GL CI study) + created plan reach to schedule study interviews

Y. Ops Product Direction Survey -> created survey development issue + created secondary analysis sissue + timeline + created video intros to project + asked for feedback:  https://youtu.be/rqdP3qI_QDs 

Y. Introduce myself at Package Eng weekly meeting -> promote ways for Eng to get involved with studies

N. Testing features meeting - cancelled


### Week of 2022-01-31
Y. Review UXR project plans prioritization with Jackie

Y. Common Screener handbook MR. 

Y. Meet with Darren to talk about research needs and plan for Q1.

Y. Work with James to Draft Script & Generate Follow-up Questions for Verify: Support external SCM (Source Code Management) with GitLab CI (Continuous Integration) 

Y. Ops Product Direction Survey  - first draft for team to respond to + draft of report + rework timeline to see if we can get a draft earlier -> then, I can run the talk aloud sessions earlier to make room for more time earlier on Enterprise Case study and use the extra participants form the common screener for that (before they expire)

Y. Last round of reruitment for Common Screener + start summary report


### Week of 2022-1-24
N. Respond to team feedback on Ops Strategy Survey - Determine Lift {Started but didn't complete}

Y. Coordinate issues posted by team in Research Prioritization Issue (goal to finalize by Feb 2 - meeting with Jackie)

N. [waiting on James to start a draft, have one now to get started] Work with James to Draft Script & Generate Follow-up Questions for Verify: Support external SCM (Source Code Management) with GitLab CI (Continuous Integration) + Build out script for other Enterprise questions from PM

Y.  Find participants for Tim and Darren's research studies via Common Screener

Y. Meet with Dov & Nadia to talk about research needs and plan for Q1. 

Y. Meet with Darren to talk about research needs and plan for Q1 - went over AutoMerge recruitment

Y. Take first steps with Common Screener Handbook MR - https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/97873

Y. See if it's porrible to recruit more Enterprise Personas via the Common Screener. Answer: No, it requires a special recruit. Created a recruitment issue: [Recruiting for #1778](https://gitlab.com/gitlab-org/ux-research/-/issues/1789) and system to use 

Extra: #253 - [Improve Merge Trains study - found participants for James + suggested framing for script](https://gitlab.com/gitlab-org/ux-research/-/issues/253#note_825388743)

### Week of 2022-1-18
1. Official Holiday - 2022-01-17
Y.  Created a research plan to integrate PM topics related to [Enterprise security case studies ](https://gitlab.com/gitlab-org/ux-research/-/issues/1778)+ and [prototype for case study reporting](https://gitlab.com/gitlab-org/ux-research/-/issues/1784)
Y. Provide a status update on Common Screener Pilot
Y. Match Screener Version - Get Updated version, wit new security focus, Tim (he will lead)- Container Registry
Y. Work on Research Prioritization Issue for Q1 FY23 - https://gitlab.com/gitlab-org/ux-research/-/issues/1750
Y.[[#1752 ](https://gitlab.com/gitlab-org/ux-research/-/issues/1752)] - Darren will lead this study, instead of me taking it to a gold support level. 
Y. [Problem Validation: General testing workflow questions #1719](https://gitlab.com/gitlab-org/ux-research/-/issues/1719) - Work with Gina to create a scalable approach to gettting customer feedback via Sales + TAMS 

### Week of 2022-1-10

1. Draft 2 more slides for UXR Presentation Deck - [Issue #1749](https://gitlab.com/gitlab-org/ux-research/-/issues/1749#note_802652382)

N. [Came up with Back-up Plan] Potential Blocker in terms of Recruitment -> Verify: Support external SCM (Source Code Management) with GitLab CI (Continuous Integration) - identify way to recruit survey responses from customer facing teams + begin sending out survey to help us get started with recruit (anticipating it will take time)

Y. Provided support for [Problem Validation: General testing workflow questions #1719](https://gitlab.com/gitlab-org/ux-research/-/issues/1719) - I picked up with this, in part, to help us develop a research program that we can use to get early feedback from Sales + TAMS

Y. Help UXR team refine draft of research prioritization process, in brainstorm doc

Y. Share / Release [Research Synthesis Summary](https://gitlab.com/gitlab-org/ux-research/-/issues/1678) Video & [Summary Deck](https://docs.google.com/presentation/d/1ypkpgNrEZEfMT4X227QT7IMzxMX_td-kovUxtNXgwhM/edit#slide=id.g98a3415fad_0_0) with team

N. [Had to focus more on recruit] Work with James to Draft Script & Generate Follow-up Questions for Verify: Support external SCM (Source Code Management) with GitLab CI (Continuous Integration) + Maybe socialize with Eng to see if they can help with any background questions that we need to ask participants about their systems

Y. Worked on survey with customer facing teams + [Provided a status update on that effort (not helpful for recruit)](https://gitlab.com/gitlab-org/ux-research/-/issues/1715#note_810233831) -> working to pivot towards a case study approach

Y. Draft research questions for 100% automated CI workflow creation study (#1752 - https://gitlab.com/gitlab-org/ux-research/-/issues/1752) 

Y. Round up responses for Match Screener Version to help us match studies with respondents + Fix any technical issues with the screener

Y. Provide Katie with feedback on Dependency Proxy study [#1638](https://gitlab.com/gitlab-org/ux-research/-/issues/1638) + meet to talk about approach for session + observed session video and provided feedback in Slack

Y. Responde to UXR team call for OKR ideas + team building opportunity canvas

Y. [Bonus] Used Common Screener to provide Katie with 3 prospective participants for Dependency Proxy Problem Validation study [#1638](https://gitlab.com/gitlab-org/ux-research/-/issues/1638) 

Y. [Bonus] Drafted reseach goal / questions + looked at recruitment criteria + created a table format to get content needed to pilot approach [[#1752 ](https://gitlab.com/gitlab-org/ux-research/-/issues/1752)]

### Week of 2022-1-04
Y. Follow-up with teams regarding the status of issues in UXR tracker + draft presentation slides for the UXR presentation. [Issue #1749](https://gitlab.com/gitlab-org/ux-research/-/issues/1749#note_802652382)

Y. Common Recruitment Screener Finalization + Created Match Screener Version to help us match studies with respondents + Drafted Screener for Respondent + Started Documenting Process for UXR partners

Y. Support for [Auto-merge solution validation](https://gitlab.com/gitlab-org/ux-research/-/issues/1568 

Y. Draft Research Issues for Prioritization (w/ Jackie): Kubecon product priority survey + in-depth interviews, first steps with secrets project, interview project to unpack what users mean by "CICD" Pipeline

Y. Verify: Support external SCM (Source Code Management) with GitLab CI (Continuous Integration) - identified and posted in slack channels to try to recruit survey responses from customer facing teams + begin sending out survey to help us get started with recruit (anticipating it will take time)

Y. Record another take of the video posting for research synthesis & reworked the slides so that they clearer calls to action for the team based on findings

Y. Met with Jackie & recorded to GitLab Unfiltered (as private)- https://youtu.be/uzWdzur5lVE

Y. Met with Darren -> to draft research goal for 100% automated CI workflow creation study (#1752 - https://gitlab.com/gitlab-org/ux-research/-/issues/1752) that we can include in the recruitment pilot & to discuss [Issue #1753](https://gitlab.com/gitlab-org/ux-research/-/issues/1753), Will GitLab SaaS Premium and Ultimate customers continue operating and managing CI build environments?

Y. Feedback on Katie's discussion guide for [Issue #1734](https://gitlab.com/gitlab-org/ux-research/-/issues/1734)

Y. Responded to Dov's CICD Templates Catalog Opporunity Canvas https://docs.google.com/document/d/12I_BbdH4kqBBCBEwsoZdvAoDfubODy-tH5ubAB0219I/edit#heading=h.4mt5fmtn0ax4

Y. Worked with Katie and Tim to align on priorities and to include Container Registry study in Common Screener Pilot

### Week of 2021-12-27
1. OOO - Holiday Break

### Week of 2021-12-21
Y. support for Verify: Support external SCM (Source Code Management) with GitLab CI (Continuous Integration) [#2314] - Drafted Goals & Project Timeline

N. Common Recruitment Screener -> format for use in Respondents

Y. Touch base with Marketing on KubeCon Europe + meeting + found stats to help us make the case for UXR attendance -> UXR will be included as stakeholders in planning process

Y. Support for analysis step with Verify:Testing - Performance Testing JTBD Research](https://gitlab.com/gitlab-org/ux-research/-/issues/1243) - Worked with Gina to refine JTDB, and those changes were merged. Drafted learnings to include in Handbook pages to help other draft them.  https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/96139

Y. Drafted discussion guide for [Auto-merge solution validation](https://gitlab.com/gitlab-org/ux-research/-/issues/1568 

Y. Include SCM to CI study in synthesis and (Reach Goal) migrate the [V&P research synthesis]((https://gitlab.com/gitlab-org/ux-research/-/issues/1678)) into a project

Y. Piloted an new approach for labeling UXR support to make it easier for teams to solicit support and know what to expect (more here -> https://gitlab.com/gitlab-org/ux-research/-/issues/1610)

 
### Week of 2021-12-13
What am I working on this week? 
Y. Primary Sprint to Dial in Recruitment plan for common screener --> [recruitment tracking issue for pilot](https://gitlab.com/gitlab-org/ux-research/-/issues/1721)

Y Requested Social Media Support for Common Screener Recruitment ->  https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/5724

Y. Create plan with Will/Lauren to optimize cross-stage support for research around secrets

Y. Support for Verify: Support external SCM (Source Code Management) with GitLab CI (Continuous Integration) [#2314](#2314) - Drafted 2 study issues ->   - [What support do Non-GitLab-SCM customers need to use GitLab CI?](https://gitlab.com/gitlab-org/ux-research/-/issues/1724) & ["Sales" Approach for GitLab CI with Non-GitLab SCM users](https://gitlab.com/gitlab-org/ux-research/-/issues/1715)

Y. Took notes during Package cleanup policies session for https://gitlab.com/gitlab-org/ux-research/-/issues/1665

Y. Reframed Research questions / goals for team to help scope study https://gitlab.com/gitlab-org/ux-research/-/issues/1638 

Y. Created How-to video for sharing live stream events for participant sessions _ https://drive.google.com/file/d/1wCnlLwD7kyABCdkIi7ljo5JTpREfC4GO/view?usp=sharing

Y. PV - light comment  / methods suggestion -> https://gitlab.com/gitlab-org/ux-research/-/issues/1719

N. Reach Goal -> SCM to CI study synthesis (reviewed all customer sessions, working on summary for synthesis)

 ### Legend
These designations are added to the previous week's priority list when adding the current week's priority.
* Y - Completed
* N - Not completed

 ### Week of 2021-12-06
What am I working on this week? 
Y CI Adoption Study Synthesis (created summary report in Dovetail and added key findings to synethesis)

Y Created an interview deck and Discussion guide for team (team might wait to execute - https://gitlab.slack.com/archives/CS4EZR8F4/p1638922105013100)

Y Refine Common Screener + Plan

Y ThinkBig - https://gitlab.com/gitlab-org/gitlab/-/issues/346881 & UXR Epic 

Y Drafted Research Plan with Veethika -> https://gitlab.com/gitlab-org/ux-research/-/issues/1568

### Template
What am I working on this week? 
1. 
1. 
1. 
Blockers? 
