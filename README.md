# Erika 🛼 🌟

## Thanks for checking in! 
I created this page to help you learn more about me and my working style. It's an evolving draft so please let me know how I can improve it. 

## Where am I from? 
- I was born in Ohio, but I've lived all over the United States as I was growing up. We moved a lot. 🚐 I like change, but I also always crave consistency. 
- My mother is an interdisciplinary academic in economics, business, innovation and the social sciences. I was brought up being asked to define my goals and provide evidence for my claims. In my world, being challenged means that I'm being respected. 
- I live in the Pacific Northwest. 🌲 🏔 🌲  And, I like to get outside as much as possible. I love hiking, rock climbing, and Glaciers are pretty neat. 
- I played Lacrosse 🥍 in college in a mid-field position. I really liked running up and down the field to keep the gameplay smooth and my team coordinated. This is a good metaphor for how I work. 

## My role
I am a [Staff UX Researcher](https://about.gitlab.com/job-families/engineering/ux-researcher/#senior-ux-researcher) for Verify and Package. I have a Ph.D. in Educational Psychology with a special focus on early childhood, cognitive development and psychometrics. Understanding children requires both quantiative and qualitative methods and I have extensive training in both. I believe that DevOps is an important scientific frontier and it fascinates me. I want to make DevOps tools easier to use so that our users can make the world a better place by innovating. 

I love research. I would like to help you with every part of research. 

I'm new to the team and am still learning about our processes and tools. 

## How I work
 - If I'm not in the mountains 🧗‍♀️ or mothering, I'm working. So, I actively block out times for family and fitness / adventures so that I stay in balance. I'm happy to take meetings whenever you can meet (as long as my schedule isn't blocked).
 - I like to be fast. I get energized by contributing to my teams and getting things done. 
 - I tend to get things done in bursts. I think and rest, sprint and repeat. 
 - I like deadlines. I love them, actually. They help me to understand how to plan out my personal sprints. Without them, I might move too quickly for my teams or just be out of pace. And, being a good team member is the most important thing to me. 
 - I am very open to feedback. 
 - I like to plan. I love to have a good plan, though that usually means that I keep changing plans to refine them. 
 - I get really focused. And, I may get so involved in tasks that I don't notice other things, like pings, or sometimes eating. This is why I schedule have to schedule downtime. 
 - I think in systems. I will usally try to solve problems in way that optimizing practice more broadly.  

## How to best work with me
- You can be really, really direct. I may actually get confused if you're not. 
- I may sound very committed to a particular idea or course of action. That's because I'm always seeking clarity. It doesn't mean that I don't want to hear other ideas. Actually, I'm usually brainstorming -- and expecting that things may change. 
- I try to plan for my personal life at least one day in advance. So, I appreciate having a day to plan for a meeting.
- Please ping me if you need me. I tend to track a lot of things, but I'm waiting for a good time to sprint on them. Please note though that I will likely starting immediately sprinting on something unless you give me a deadline or help me to understand what I should be waiting for. 

## Known failure modes
 - I work too much and it can take a toll on my personal life. 
 - I tend to give myself more time than I need when planning projects. This is because it is stressful for me to not deliver on something that I said I would do. Keep in mind that I might get something done in half the time that I allotted, and that I might ask for more time than I need. Just let me know it's ok if I don't deliver or we need to delay and I'll set a more aggressive timeline. 
